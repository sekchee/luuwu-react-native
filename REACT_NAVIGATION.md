* create App/Navigators folder, and AppNavigator.js (from luuwu boilerplate)
* https://reactnavigation.org/docs/en/getting-started.html
```
npm install react-navigation react-native-reanimated react-native-gesture-handler react-native-screens
npm install react-navigation-stack
npm install react-navigation-tabs
```
* https://reactnavigation.org/docs/en/hello-react-navigation.html

# IOS
* To complete the linking on iOS, 

```
cd ios
pod install
cd ..
```

# Android
- To finalize installation of react-native-screens for Android, add the following two lines to dependencies section in android/app/build.gradle:
```
implementation 'androidx.appcompat:appcompat:1.1.0-rc01'
implementation 'androidx.swiperefreshlayout:swiperefreshlayout:1.1.0-alpha02'
```

- To finalize installation of react-native-gesture-handler for Android, make the following modifications to MainActivity.java (in android/app/src/main/java):

```
package com.reactnavigation.example;

import com.facebook.react.ReactActivity;
+ import com.facebook.react.ReactActivityDelegate;
+ import com.facebook.react.ReactRootView;
+ import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;

public class MainActivity extends ReactActivity {

  @Override
  protected String getMainComponentName() {
    return "Example";
  }

+  @Override
+  protected ReactActivityDelegate createReactActivityDelegate() {
+    return new ReactActivityDelegate(this, getMainComponentName()) {
+      @Override
+      protected ReactRootView createRootView() {
+       return new RNGestureHandlerEnabledRootView(MainActivity.this);
+      }
+    };
+  }
}
```