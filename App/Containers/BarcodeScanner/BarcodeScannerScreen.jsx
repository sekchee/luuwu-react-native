import React from 'react';
import { PropTypes } from 'prop-types';
import { View, TouchableOpacity, Dimensions, ImageBackground } from 'react-native';
import { Button, Text, Card } from 'react-native-elements';
import { connect } from 'react-redux';
import { RNCamera } from 'react-native-camera';
import { SafeAreaView } from 'react-navigation';
import Style from './BarcodeScannerStyle';
import BarcodeScannerActions from '../../Stores/BarcodeScanner/Actions';
import {
  translate,
  setI18nConfig,
  addLocaleChangedListener,
  removeLocaleChangedListener
} from '../../Translations';

const flashModeOrder = {
  off: 'torch',
  torch: 'off'
};

class BarcodeScannerScreen extends React.PureComponent {
  constructor() {
    super();

    setI18nConfig(); // set initial config

    this.state = {
      height: Dimensions.get('window').height,
      width: Dimensions.get('window').width,
      flash: 'off',
      zoom: 0,
      autoFocus: RNCamera.Constants.AutoFocus.on,
      depth: 1,
      type: 'back',
      whiteBalance: 'auto',
      ratio: '16:9',
      canDetectBarcode: true,
      barcodes: {},
      selectedBarcode: '',
      redLinePosition: {},
      selectedBarcodeImage: '',
      imageComponentWidth: 0,
      imageComponentHeight: 0,
      smallBarcodeBound: {},
      buttonTitle: 'Scanning'
    };

    this.useZoomIn = this.useZoomIn.bind(this);
    this.useZoomOut = this.useZoomOut.bind(this);
    this.useToggleFlash = this.useToggleFlash.bind(this);
    this.useConfirmBarcode = this.useConfirmBarcode.bind(this);
    this.useOnBarcodeRecognized = this.useOnBarcodeRecognized.bind(this);
    this.useOnLocalizationChange = this.useOnLocalizationChange.bind(this);
    this.storeRedLinePosition = this.storeRedLinePosition.bind(this);
    this.setPicture = this.setPicture.bind(this);
  }

  componentDidMount() {
    addLocaleChangedListener(this.handleLocalizationChange);
  }

  componentWillUnmount() {
    removeLocaleChangedListener(this.handleLocalizationChange);
  }

  async setPicture() {
    if (this.camera) {
      const imageData = await this.camera.takePictureAsync({ quality: 0.5 });
      this.setState({
        selectedBarcodeImage: imageData.uri
      });
    }
  }

  useOnLocalizationChange() {
    setI18nConfig();
    this.forceUpdate();
  }

  useToggleFlash() {
    const { flash } = this.state;
    this.setState({
      flash: flashModeOrder[flash]
    });
  }

  useZoomOut() {
    const { zoom } = this.state;
    this.setState({
      zoom: zoom - 0.1 < 0 ? 0 : zoom - 0.1
    });
  }

  useZoomIn() {
    const { zoom } = this.state;
    this.setState({
      zoom: zoom + 0.1 > 1 ? 1 : zoom + 0.1
    });
  }

  useOnBarcodeRecognized({ barcodes }) {
    const { barcodes: oldBarcodes, redLinePosition, selectedBarcode } = this.state;
    const processedBarcodes = {};
    const curTimestamp = Date.now();

    // loop the barcodes first, barcodes is an array, not object
    for (let a = 0; a < barcodes.length; a += 1) {
      const tmpBarcode = barcodes[a];

      const redLineX = (redLinePosition.x + redLinePosition.width) / 2;
      const redLineY = (redLinePosition.y + redLinePosition.height) / 2;
      // check if this barcode bound touch the red line
      if (
        redLineY >= tmpBarcode.bounds.origin.y &&
        redLineY <= tmpBarcode.bounds.origin.y + tmpBarcode.bounds.size.height
      ) {
        // calculate the distance to center point
        const centerX = tmpBarcode.bounds.origin.x + tmpBarcode.bounds.size.width / 2;
        const centerY = tmpBarcode.bounds.origin.y + tmpBarcode.bounds.size.height / 2;
        const distX = centerX - redLineX;
        const distY = centerY - redLineY;
        const distance = Math.sqrt(distX * distX + distY * distY);

        const key = tmpBarcode.data;
        processedBarcodes[key] = {
          ...tmpBarcode,
          timestamp: curTimestamp,
          isSelected: false,
          distance
        };

        // check with oldBarcodes
        if (Object.prototype.hasOwnProperty.call(oldBarcodes, key)) {
          delete oldBarcodes[key];
        }
      }
    }

    // loop the remained oldBarcodes, oldBarcodes is an key/value object (associated array)
    Object.keys(oldBarcodes).forEach(key => {
      const tmpBarcode = oldBarcodes[key];

      // verify the timestamp, if lesser than or equal to 1 second, add to processedBarcodes if not just remove it
      // the timestamp is on miliseconds
      const seconds = curTimestamp - tmpBarcode.timestamp;
      if (seconds <= 500) {
        processedBarcodes[key] = {
          ...tmpBarcode
        };
      }
    });

    // loop processedBarcodes, check smallest distance
    let smallestDistance = Number.MAX_VALUE;
    Object.keys(processedBarcodes).forEach(key => {
      const tmpBarcode = processedBarcodes[key];

      if (tmpBarcode.distance < smallestDistance) {
        smallestDistance = tmpBarcode.distance;
      }
    });
    // set the smallest distance to isSelected
    Object.keys(processedBarcodes).some(key => {
      const tmpBarcode = processedBarcodes[key];

      if (tmpBarcode.distance === smallestDistance) {
        tmpBarcode.isSelected = true;
        if (tmpBarcode.data !== selectedBarcode) {
          this.state.buttonTitle = 'Accept';
          this.setPicture();
          this.setState({
            selectedBarcode: tmpBarcode.data
          });
        }
        return true;
      }
      return false;
    });

    this.setState({
      barcodes: processedBarcodes
    });
  }

  useConfirmBarcode() {
    const { scanSuccess } = this.props;
    const { selectedBarcode } = this.state;
    scanSuccess([selectedBarcode]);
  }

  storeRedLinePosition({ nativeEvent: { layout } }) {
    this.setState({
      redLinePosition: layout
    });
  }

  renderBarcodes() {
    const { barcodes } = this.state;
    return (
      <View style={Style.barcodesContainer}>
        {Object.values(barcodes).map(({ bounds, data, isSelected }) => {
          return (
            <View
              key={data}
              style={[
                isSelected === true ? Style.selectedText : Style.text,
                {
                  ...bounds.size,
                  left: bounds.origin.x,
                  top: bounds.origin.y
                }
              ]}
            />
          );
        })}
      </View>
    );
  }

  renderCamera() {
    const {
      canDetectBarcode,
      type,
      flash,
      autoFocus,
      zoom,
      whiteBalance,
      ratio,
      depth,
      selectedBarcode,
      selectedBarcodeImage,
      imageComponentWidth,
      imageComponentHeight,
      barcodes,
      smallBarcodeBound,
      redLinePosition,
      buttonTitle
    } = this.state;
    return (
      <SafeAreaView style={Style.safeView}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={{
            flex: 1
          }}
          type={type}
          flashMode={flash}
          autoFocus={autoFocus}
          zoom={zoom}
          whiteBalance={whiteBalance}
          ratio={ratio}
          focusDepth={depth}
          trackingEnabled
          androidCameraPermissionOptions={{
            title: translate('permission_to_use_camera'),
            message: translate('We_need_your_permission_to_use_your_camera'),
            buttonPositive: translate('ok'),
            buttonNegative: translate('cancel')
          }}
          faceDetectionLandmarks={undefined}
          faceDetectionClassifications={undefined}
          onFacesDetected={null}
          onTextRecognized={null}
          onGoogleVisionBarcodesDetected={canDetectBarcode ? this.useOnBarcodeRecognized : null}
          googleVisionBarcodeType={RNCamera.Constants.GoogleVisionBarcodeDetection.BarcodeType.ALL}
          captureAudio={false}
        >
          <View
            style={{
              flex: 0.5
            }}
          >
            <View
              style={{
                backgroundColor: 'transparent',
                flexDirection: 'row',
                justifyContent: 'center'
              }}
            >
              <TouchableOpacity style={Style.flipButton} onPress={this.useToggleFlash}>
                <Text style={Style.flipText}>
                  {' '}
                  {translate('flash')}:{flash}{' '}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[Style.flipButton, { flex: 0.1, alignSelf: 'flex-end' }]}
                onPress={this.useZoomIn}
              >
                <Text style={Style.flipText}> + </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[Style.flipButton, { flex: 0.1, alignSelf: 'flex-end' }]}
                onPress={this.useZoomOut}
              >
                <Text style={Style.flipText}> - </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View
            onLayout={this.storeRedLinePosition}
            style={{
              flex: 0.5,
              borderTopColor: 'red',
              borderTopWidth: 1
            }}
          >
            <View style={Style.footerContainer}>
              {selectedBarcode.length === 0 ? null : (
                <Card
                  title="Barcode Detected"
                  titleStyle={{ marginTop: 15 }}
                  dividerStyle={{ height: 0, marginBottom: 0 }}
                  containerStyle={{ padding: 0 }}
                >
                  <ImageBackground
                    source={
                      selectedBarcodeImage.length !== 0 ? { uri: selectedBarcodeImage } : null
                    }
                    style={{ height: 150, overflow: 'hidden' }}
                    onLayout={event => {
                      this.setState({
                        imageComponentWidth: event.nativeEvent.layout.width,
                        imageComponentHeight: event.nativeEvent.layout.height
                      });
                    }}
                    onLoad={() => {
                      if (barcodes[selectedBarcode]) {
                        // imageComponentWidth is also the width after resize
                        const shrinkRatio =
                          imageComponentWidth / (redLinePosition.x + redLinePosition.width);
                        const aspectRatio =
                          (redLinePosition.x + redLinePosition.width) /
                          (redLinePosition.y + redLinePosition.height);

                        // Aspect ratio before and after resize is the same
                        const fullHeightAfterResize = imageComponentWidth / aspectRatio;
                        const boundX = barcodes[selectedBarcode].bounds.origin.x;
                        const boundY = barcodes[selectedBarcode].bounds.origin.y;
                        const newX = boundX * shrinkRatio;
                        const newY =
                          boundY * shrinkRatio - (fullHeightAfterResize - imageComponentHeight) / 2;
                        const newWidth = barcodes[selectedBarcode].bounds.size.width * shrinkRatio;
                        const newHeight =
                          barcodes[selectedBarcode].bounds.size.height * shrinkRatio;
                        this.setState({
                          smallBarcodeBound: {
                            x: newX,
                            y: newY,
                            height: newHeight,
                            width: newWidth
                          }
                        });
                      }
                    }}
                  >
                    <View
                      style={{
                        borderWidth: 2,
                        width: smallBarcodeBound.width,
                        height: smallBarcodeBound.height,
                        position: 'absolute',
                        left: smallBarcodeBound.x,
                        top: smallBarcodeBound.y,
                        borderColor: '#FF0000'
                      }}
                    />
                  </ImageBackground>
                  <View style={{ padding: 10 }}>
                    <Text style={{ marginBottom: 10 }}>{selectedBarcode}</Text>
                  </View>
                </Card>
              )}
            </View>
          </View>
          {zoom !== 0 && <Text style={[Style.flipText, Style.zoomText]}>Zoom: {zoom}</Text>}
          {canDetectBarcode && this.renderBarcodes()}
        </RNCamera>
        <Button
          buttonStyle={{
            borderRadius: 0,
            marginLeft: 0,
            marginRight: 0,
            marginBottom: 0
          }}
          title={buttonTitle}
          onPress={this.useConfirmBarcode}
          disabled={buttonTitle === 'Scanning'}
        />
      </SafeAreaView>
    );
  }

  render() {
    const { height, width } = this.state;
    return (
      <View style={Style.container}>
        <View
          style={[
            Style.cameraContainer,
            {
              height,
              width
            }
          ]}
        >
          {this.renderCamera()}
        </View>
      </View>
    );
  }
}

BarcodeScannerScreen.propTypes = {
  scanSuccess: PropTypes.func
};

BarcodeScannerScreen.defaultProps = {
  scanSuccess: () => {}
};

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  scanSuccess: barcodes => dispatch(BarcodeScannerActions.scanSuccess(barcodes))
});

export default connect(mapStateToProps, mapDispatchToProps)(BarcodeScannerScreen);
