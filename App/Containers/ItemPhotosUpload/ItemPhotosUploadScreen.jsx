import React from 'react';
import { View, SafeAreaView, FlatList, ActivityIndicator } from 'react-native';
import { Input, Image, Button, Text } from 'react-native-elements';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import * as Yup from 'yup';
import { Formik } from 'formik';
import ItemDetailsActions from '../../Stores/ItemDetails/Actions';
import Style from './ItemPhotosUploadStyle';
import {
  translate,
  setI18nConfig,
  addLocaleChangedListener,
  removeLocaleChangedListener
} from '../../Translations';

class ItemPhotosUploadScreen extends React.PureComponent {
  constructor() {
    super();

    setI18nConfig(); // set initial config

    this.useOnLocalizationChange = this.useOnLocalizationChange.bind(this);
    this.useStopUploadItemPhoto = this.useStopUploadItemPhoto.bind(this);
    this.renderItem = this.renderItem.bind(this);
  }

  componentDidMount() {
    addLocaleChangedListener(this.handleLocalizationChange);
  }

  componentDidUpdate() {}

  componentWillUnmount() {
    removeLocaleChangedListener(this.handleLocalizationChange);

    /*
    ImagePicker.clean().then(() => {
      
    }).catch(e => {
      
    });
    */
  }

  useOnLocalizationChange() {
    setI18nConfig();
    this.forceUpdate();
  }

  useStopUploadItemPhoto() {
    const { stopUploadItemPhoto } = this.props;
    stopUploadItemPhoto();
  }

  renderItem({ item, index }) {
    const { itemId: propsItemId, uploadItemPhoto } = this.props;
    const initialValues = {
      itemId: propsItemId,
      desc01: '',
      desc02: ''
    };

    return (
      <Formik
        enableReinitialize
        initialValues={initialValues}
        onSubmit={(values, formikBag) => {
          const { itemId, desc01, desc02 } = values;
          const isFeatured = false;
          const uomId = 35;
          // dispatch the action
          uploadItemPhoto(formikBag, index, itemId, isFeatured, uomId, desc01, desc02, item);
        }}
        validationSchema={Yup.object().shape({})}
      >
        {({ values, handleChange, errors, isSubmitting, isValid, handleSubmit }) => (
          <View style={Style.formContainer}>
            <Image
              style={Style.mediumImage}
              source={{
                uri: item.path,
                width: item.width,
                height: item.height,
                mime: item.mime
              }}
              resizeMode="contain"
              PlaceholderContent={<ActivityIndicator />}
            />
            <Input
              containerStyle={Style.formElement}
              value={values.desc01}
              onChangeText={handleChange('desc01')}
              placeholder={translate('desc01')}
              errorMessage={errors.desc_01}
            />
            <Input
              containerStyle={Style.formElement}
              value={values.desc02}
              onChangeText={handleChange('desc02')}
              placeholder={translate('desc02')}
              secureTextEntry
              errorMessage={errors.desc_02}
            />
            <Button
              containerStyle={Style.formElement}
              disabled={!isValid}
              loading={isSubmitting}
              onPress={handleSubmit}
              title={translate('upload')}
            />
          </View>
        )}
      </Formik>
    );
  }

  static renderSeparator() {
    return (
      <View
        style={{
          borderBottomColor: 'black',
          borderBottomWidth: 1
        }}
      />
    );
  }

  render() {
    const { successMessage, errorMessage, selectedPhotos } = this.props;
    return (
      <SafeAreaView style={Style.container}>
        {successMessage !== '' && <Text style={Style.successText}>{successMessage}</Text>}
        {errorMessage !== '' && <Text style={Style.errorText}>{errorMessage}</Text>}
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          data={selectedPhotos}
          renderItem={this.renderItem}
          ItemSeparatorComponent={ItemPhotosUploadScreen.renderSeparator}
          ListEmptyComponent={
            <View style={Style.noPhotoContainer}>
              <Text style={Style.noPhotoText}>{translate('no_photo_to_upload')}</Text>
              <Button style={{}} onPress={this.useStopUploadItemPhoto} title={translate('back')} />
            </View>
          }
        />
      </SafeAreaView>
    );
  }
}

ItemPhotosUploadScreen.propTypes = {
  itemId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  selectedPhotos: PropTypes.arrayOf(
    PropTypes.shape({
      modificationDate: PropTypes.string,
      size: PropTypes.number,
      mime: PropTypes.string,
      height: PropTypes.number,
      width: PropTypes.number,
      path: PropTypes.string
    })
  ),
  successMessage: PropTypes.string,
  errorMessage: PropTypes.string,
  uploadItemPhoto: PropTypes.func,
  stopUploadItemPhoto: PropTypes.func
};

ItemPhotosUploadScreen.defaultProps = {
  itemId: 0,
  selectedPhotos: [],
  successMessage: '',
  errorMessage: '',
  uploadItemPhoto: () => {},
  stopUploadItemPhoto: () => {}
};

const mapStateToProps = state => ({
  itemId: state.itemDetails.itemId,
  selectedPhotos: state.itemDetails.selectedPhotos,
  successMessage: state.itemDetails.successMessage,
  errorMessage: state.itemDetails.errorMessage
});

const mapDispatchToProps = dispatch => ({
  uploadItemPhoto: (formikBag, itemPhotoIndex, itemId, isFeatured, uomId, desc01, desc02, photo) =>
    dispatch(
      ItemDetailsActions.uploadItemPhoto(
        formikBag,
        itemPhotoIndex,
        itemId,
        isFeatured,
        uomId,
        desc01,
        desc02,
        photo
      )
    ),
  stopUploadItemPhoto: () => dispatch(ItemDetailsActions.stopUploadItemPhoto())
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemPhotosUploadScreen);
