import React from 'react';
import { View, SafeAreaView } from 'react-native';
import { Input, Image, Button, Text, Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import treeChanges from 'tree-changes';
import { Formik } from 'formik';
import * as Yup from 'yup';
import AppActions from '../../Stores/App/Actions';
import BarcodeScannerActions from '../../Stores/BarcodeScanner/Actions';
import Style from './LoginStyle';
import { Images } from '../../Theme';
import {
  translate,
  setI18nConfig,
  addLocaleChangedListener,
  removeLocaleChangedListener
} from '../../Translations';

class LoginScreen extends React.PureComponent {
  constructor() {
    super();

    setI18nConfig(); // set initial config

    this.state = {
      username: '',
      password: '',
      scannerToken: Date.now()
    };

    this.useOnScanBarcode = this.useOnScanBarcode.bind(this);
    this.processSyncQrCode = this.processSyncQrCode.bind(this);
    this.useOnLocalizationChange = this.useOnLocalizationChange.bind(this);
  }

  componentDidMount() {
    addLocaleChangedListener(this.handleLocalizationChange);
  }

  componentDidUpdate(prevProps) {
    const { changedFrom } = treeChanges(prevProps, this.props);
    const { scannerToken: stateScannerToken } = this.state;
    const { scannerToken: propsScannerToken, barcodes } = this.props;
    if (stateScannerToken === propsScannerToken && changedFrom('scannerIsSuccess', false, true)) {
      this.processSyncQrCode(barcodes);
    }
  }

  componentWillUnmount() {
    removeLocaleChangedListener(this.handleLocalizationChange);
  }

  useOnLocalizationChange() {
    setI18nConfig();
    this.forceUpdate();
  }

  useOnScanBarcode() {
    const { startScanner } = this.props;
    const { scannerToken } = this.state;
    // scanner token is to identify the scan result is belong to who
    startScanner(scannerToken);
  }

  processSyncQrCode(barcodes) {
    const { updateApiUrl, showErrorMessage } = this.props;
    if (barcodes.length > 0) {
      const barcode = barcodes[0];
      const codeParts = barcode.split(';');
      if (codeParts.length === 4) {
        const operator = codeParts[0];
        const username = codeParts[1];
        const password = codeParts[2];
        const apiUrl = codeParts[3];

        if (operator === 'SYNC') {
          this.setState({
            username,
            password
          });
          updateApiUrl(apiUrl);
        } else {
          showErrorMessage(translate('incorrect_qr_code_format'));
        }
      } else {
        showErrorMessage(translate('incorrect_qr_code_format'));
      }
    } else {
      showErrorMessage(translate('no_barcode_detected'));
    }
  }

  render() {
    const { username, password } = this.state;
    const { apiUrl, successMessage, errorMessage, authenticate } = this.props;
    const initialValues = {
      username,
      password,
      apiUrl
    };

    return (
      <SafeAreaView style={Style.container}>
        {successMessage !== '' && (
          <View style={Style.successContainer}>
            <Text style={Style.successText}>{successMessage}</Text>
          </View>
        )}
        {errorMessage !== '' && (
          <View style={Style.errorContainer}>
            <Text style={Style.errorText}>{errorMessage}</Text>
          </View>
        )}
        <View style={Style.logoContainer}>
          <Image source={Images.logo} style={Style.logoImage} />
        </View>
        <View style={Style.formContainer}>
          <Formik
            enableReinitialize
            initialValues={initialValues}
            onSubmit={(values, formikBag) => {
              // save to state
              this.setState({
                username: values.username,
                password: values.password
              });
              // dispatch the action
              // Use values.username and values.password because setState is asynchronous
              // Therefore, the username and password in the state might not have changed when executing this line
              authenticate(formikBag, values.username, values.password);
            }}
            validationSchema={Yup.object().shape({
              username: Yup.string().required(translate('username_is_required')),
              password: Yup.string().required(translate('password_is_required')),
              apiUrl: Yup.string().required(translate('url_is_required'))
            })}
          >
            {({ values, handleChange, errors, isSubmitting, isValid, handleSubmit }) => (
              <>
                <Input
                  containerStyle={Style.formElement}
                  value={values.username}
                  onChangeText={handleChange('username')}
                  placeholder={translate('username')}
                  errorMessage={errors.username}
                  disabled={isSubmitting}
                />
                <Input
                  containerStyle={Style.formElement}
                  value={values.password}
                  onChangeText={handleChange('password')}
                  placeholder={translate('password')}
                  secureTextEntry
                  errorMessage={errors.password}
                  disabled={isSubmitting}
                />
                <Input
                  containerStyle={Style.formElement}
                  value={values.apiUrl}
                  onChangeText={handleChange('apiUrl')}
                  placeholder={translate('url')}
                  errorMessage={errors.apiUrl}
                  multiline
                  rightIcon={
                    <Button
                      icon={<Icon name="barcode-scan" type="material-community" size={25} />}
                      onPress={this.useOnScanBarcode}
                    />
                  }
                  disabled={isSubmitting}
                />
                <Button
                  containerStyle={Style.formElement}
                  disabled={!isValid}
                  loading={isSubmitting}
                  onPress={handleSubmit}
                  title={translate('login')}
                />
              </>
            )}
          </Formik>
        </View>
      </SafeAreaView>
    );
  }
}

LoginScreen.propTypes = {
  scannerToken: PropTypes.number,
  apiUrl: PropTypes.string,
  successMessage: PropTypes.string,
  errorMessage: PropTypes.string,
  barcodes: PropTypes.arrayOf(PropTypes.string),
  showErrorMessage: PropTypes.func,
  updateApiUrl: PropTypes.func,
  startScanner: PropTypes.func,
  authenticate: PropTypes.func
};

LoginScreen.defaultProps = {
  scannerToken: 0,
  apiUrl: '',
  successMessage: '',
  errorMessage: '',
  barcodes: [],
  showErrorMessage: () => {},
  updateApiUrl: () => {},
  startScanner: () => {},
  authenticate: () => {}
};

const mapStateToProps = state => ({
  apiUrl: state.app.apiUrl,
  successMessage: state.app.successMessage,
  errorMessage: state.app.errorMessage,
  scannerToken: state.barcodeScanner.token,
  barcodes: state.barcodeScanner.barcodes,
  scannerIsSuccess: state.barcodeScanner.scannerIsSuccess
});

const mapDispatchToProps = dispatch => ({
  authenticate: (formikBag, username, password) =>
    dispatch(AppActions.authenticate(formikBag, username, password)),
  showErrorMessage: errorMessage => dispatch(AppActions.showErrorMessage(errorMessage)),
  updateApiUrl: apiUrl => dispatch(AppActions.updateApiUrl(apiUrl)),
  startScanner: token => dispatch(BarcodeScannerActions.startScanner(token))
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
