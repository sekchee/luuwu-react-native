import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  formContainer: {
    flex: 0.7,
    flexDirection: 'column',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10
  },
  formElement: {
    marginTop: 10
  },
  errorContainer: {
    flex: 0.1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFB6C1'
  },
  errorText: {
    color: 'red'
  },
  successContainer: {
    flex: 0.1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#98FB98'
  },
  successText: {
    color: 'green'
  }
});
