import React, { Component } from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { ThemeProvider } from 'react-native-elements';
import NavigationService from '../../Services/NavigationService';
import AppNavigator from '../../Navigators/AppNavigator';
import StartupActions from '../../Stores/Startup/Actions';

const theme = {
  Button: {
    type: 'outline',
    raised: true
  }
};

class RootScreen extends Component {
  componentDidMount() {
    const { startup } = this.props;
    // Run the startup saga when the application is starting
    startup();
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <AppNavigator
          // Initialize the NavigationService (see https://reactnavigation.org/docs/en/navigating-without-navigation-prop.html)
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </ThemeProvider>
    );
  }
}

RootScreen.propTypes = {
  startup: PropTypes.func
};

RootScreen.defaultProps = {
  startup: () => {}
};

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  startup: () => dispatch(StartupActions.startup())
});

export default connect(mapStateToProps, mapDispatchToProps)(RootScreen);
