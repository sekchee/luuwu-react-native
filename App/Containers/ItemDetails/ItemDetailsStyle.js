import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  scrollView: {},
  topBarContainer: {
    flex: 0.1,
    flexDirection: 'row',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10
  },
  mediumImage: {
    width: 200,
    height: 250
  },
  errorContainer: {
    flex: 0.1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFB6C1'
  },
  errorText: {
    marginLeft: 10,
    color: 'red'
  },
  successContainer: {
    flex: 0.1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#98FB98'
  },
  successText: {
    marginLeft: 10,
    color: 'green'
  }
});
