import React from 'react';
import { View, ScrollView, SafeAreaView, FlatList, ActivityIndicator } from 'react-native';
import { ListItem, Image, Text, Button, Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import ImagePicker from 'react-native-image-crop-picker';
import ItemDetailsActions from '../../Stores/ItemDetails/Actions';
import Style from './ItemDetailsStyle';
import { Images } from '../../Theme';
import {
  setI18nConfig,
  addLocaleChangedListener,
  removeLocaleChangedListener
} from '../../Translations';

class ItemDetailsScreen extends React.PureComponent {
  constructor() {
    super();

    setI18nConfig(); // set initial config

    this.useOnLocalizationChange = this.useOnLocalizationChange.bind(this);
    this.useOnItemPhotoPress = this.useOnItemPhotoPress.bind(this);
    this.useOpenPicker = this.useOpenPicker.bind(this);
    this.useOpenCamera = this.useOpenCamera.bind(this);
    this.renderItemPhoto = this.renderItemPhoto.bind(this);
  }

  componentDidMount() {
    addLocaleChangedListener(this.handleLocalizationChange);

    const {
      resetItemPhotos,
      fetchItemPhotos,
      itemId,
      itemPhotosSorts,
      itemPhotosFilters,
      itemPhotosPageSize
    } = this.props;
    // fetch item first page
    resetItemPhotos();
    fetchItemPhotos(itemId, 1, itemPhotosSorts, itemPhotosFilters, itemPhotosPageSize);
  }

  componentDidUpdate() {}

  componentWillUnmount() {
    removeLocaleChangedListener(this.handleLocalizationChange);
  }

  useOnLocalizationChange() {
    setI18nConfig();
    this.forceUpdate();
  }

  useOpenPicker() {
    ImagePicker.openPicker({
      multiple: true,
      mediaType: 'photo'
    })
      .then(images => {
        // console.log(images);
        const { selectPhotos } = this.props;
        selectPhotos(images);
      })
      .catch(() => {
        // console.log("useOpenPicker catch" + err.toString())
      });
  }

  useOpenCamera() {
    ImagePicker.openCamera({
      mediaType: 'photo'
      // includeBase64: true
    })
      .then(image => {
        const { selectPhotos } = this.props;
        selectPhotos([image]);
      })
      .catch(() => {
        // console.log("useOpenCamera catch" + err.toString())
      });
  }

  // REMOVE COMMENTS AFTER THE IMPLEMENTATION FOR THE FUNCTION IS CODED
  /* eslint-disable class-methods-use-this */
  // eslint-disable-next-line no-unused-vars
  useOnItemPhotoPress(item, index) {}
  /* eslint-enable class-methods-use-this */

  renderItemPhoto({ item }) {
    return (
      <ListItem
        key={item.id}
        leftElement={(
          <View>
            <Image
              source={item.medium_url ? { uri: item.medium_url } : Images.noPhotoMedium}
              style={Style.mediumImage}
              resizeMode="contain"
              PlaceholderContent={<ActivityIndicator />}
            />
          </View>
        )}
        title={(
          <View>
            <Text h4>{item.desc_01}</Text>
          </View>
        )}
        subtitle={(
          <View>
            <Text>{item.desc_02}</Text>
          </View>
        )}
        onPress={this.useOnItemPhotoPress}
        bottomDivider
      />
    );
  }

  render() {
    const { successMessage, errorMessage, itemPhotos, itemPhotosIsLoading } = this.props;
    return (
      <SafeAreaView style={Style.container}>
        {successMessage !== '' && (
          <View style={Style.successContainer}>
            <Text style={Style.successText}>{successMessage}</Text>
          </View>
        )}
        {errorMessage !== '' && (
          <View style={Style.errorContainer}>
            <Text style={Style.errorText}>{errorMessage}</Text>
          </View>
        )}
        <ScrollView style={Style.scrollView}>
          <View style={Style.topBarContainer}>
            <Button
              icon={<Icon name="folder-upload" type="material-community" size={25} />}
              onPress={this.useOpenPicker}
            />
            <Button
              icon={<Icon name="camera" type="material-community" size={25} />}
              onPress={this.useOpenCamera}
            />
          </View>
          <FlatList
            keyExtractor={item => item.id.toString()}
            data={itemPhotos}
            renderItem={this.renderItemPhoto}
            refreshing={itemPhotosIsLoading}
            ListEmptyComponent={(
              <Image
                source={Images.noPhotoMedium}
                style={Style.mediumImage}
                PlaceholderContent={<ActivityIndicator />}
              />
            )}
          />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

ItemDetailsScreen.propTypes = {
  itemId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  itemPhotos: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      line_no: PropTypes.number,
      item_id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      uom_id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      file_ext: PropTypes.string,
      desc_01: PropTypes.string,
      desc_02: PropTypes.string,
      old_filename: PropTypes.string,
      old_width: PropTypes.number,
      old_height: PropTypes.number,
      created_at: PropTypes.string,
      updated_at: PropTypes.string,
      icon_url: PropTypes.string,
      medium_url: PropTypes.string,
      large_url: PropTypes.string
    })
  ),
  itemPhotosIsLoading: PropTypes.bool,
  itemPhotosSorts: PropTypes.shape({}),
  itemPhotosFilters: PropTypes.shape({}),
  itemPhotosPageSize: PropTypes.number,
  successMessage: PropTypes.string,
  errorMessage: PropTypes.string,
  resetItemPhotos: PropTypes.func,
  fetchItemPhotos: PropTypes.func,
  selectPhotos: PropTypes.func
};

ItemDetailsScreen.defaultProps = {
  itemId: 0,
  itemPhotos: [],
  itemPhotosIsLoading: false,
  itemPhotosSorts: PropTypes.shape({}),
  itemPhotosFilters: PropTypes.shape({}),
  itemPhotosPageSize: 0,
  successMessage: '',
  errorMessage: '',
  resetItemPhotos: () => {},
  fetchItemPhotos: () => {},
  selectPhotos: () => {}
};

const mapStateToProps = state => ({
  itemId: state.itemDetails.itemId,
  itemPhotos: state.itemDetails.itemPhotos,
  itemPhotosIsLoading: state.itemDetails.itemPhotosIsLoading,
  itemPhotosSorts: state.itemDetails.itemPhotosSorts,
  itemPhotosFilters: state.itemDetails.itemPhotosFilters,
  itemPhotosCurrentPage: state.itemDetails.itemPhotosCurrentPage,
  itemPhotosPageSize: state.itemDetails.itemPhotosPageSize,
  successMessage: state.itemDetails.successMessage,
  errorMessage: state.itemDetails.errorMessage
});

const mapDispatchToProps = dispatch => ({
  resetItemPhotos: () => dispatch(ItemDetailsActions.resetItemPhotos()),
  fetchItemPhotos: (itemId, page, sorts, filters, pageSize) =>
    dispatch(ItemDetailsActions.fetchItemPhotos(itemId, page, sorts, filters, pageSize)),
  selectPhotos: photos => dispatch(ItemDetailsActions.selectPhotos(photos))
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemDetailsScreen);
