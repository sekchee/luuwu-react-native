import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  safeView: {
    flex: 1,
    flexDirection: 'column'
  },
  topBarContainer: {
    flex: 0.1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20
  },
  container: {
    flex: 0.9,
    flexDirection: 'column',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10
  },
  filterButtonsContainer: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 30
  },
  title: {
    marginTop: 10
  },
  divider: {
    marginTop: 30
  },
  selectButtonContainer: {
    marginTop: 10
  }
});
