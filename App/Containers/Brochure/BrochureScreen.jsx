import React from 'react';
import { View, SafeAreaView, FlatList, ActivityIndicator } from 'react-native';
import { ListItem, Image, Text, Button, Avatar, Icon, Badge } from 'react-native-elements';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import treeChanges from 'tree-changes';
import BrochureActions from '../../Stores/Brochure/Actions';
import ItemDetailsActions from '../../Stores/ItemDetails/Actions';
import BarcodeScannerActions from '../../Stores/BarcodeScanner/Actions';
import Style from './BrochureStyle';
import FiltersDialog from './FiltersDialog';
import { Images } from '../../Theme';
import {
  translate,
  setI18nConfig,
  addLocaleChangedListener,
  removeLocaleChangedListener
} from '../../Translations';

class BrochureScreen extends React.PureComponent {
  constructor() {
    super();

    this.state = {
      isFilterVisible: false,
      scannerToken: Date.now()
    };

    setI18nConfig(); // set initial config

    this.useOnLocalizationChange = this.useOnLocalizationChange.bind(this);
    this.useOnEndReached = this.useOnEndReached.bind(this);
    this.useOnRefresh = this.useOnRefresh.bind(this);
    this.useShowFilters = this.useShowFilters.bind(this);
    this.useOnScanBarcode = this.useOnScanBarcode.bind(this);

    this.renderItem = this.renderItem.bind(this);

    this.selectItem = this.selectItem.bind(this);
  }

  componentDidMount() {
    addLocaleChangedListener(this.handleLocalizationChange);

    // fetch item first page
    const { resetItems, fetchItems, sorts, filters, pageSize } = this.props;
    resetItems();
    fetchItems(1, sorts, filters, pageSize);
  }

  componentDidUpdate(prevProps) {
    const { changedFrom, changed } = treeChanges(prevProps, this.props);
    const { scannerToken } = this.state;
    const {
      scannerToken: propScannerToken,
      resetItems,
      fetchItems,
      barcodes,
      sorts,
      filters,
      pageSize
    } = this.props;

    if (scannerToken === propScannerToken && changedFrom('scannerIsSuccess', false, true)) {
      this.processBarcode(barcodes);
    }

    if (changed('filters')) {
      resetItems();
      fetchItems(1, sorts, filters, pageSize);
    }
  }

  componentWillUnmount() {
    removeLocaleChangedListener(this.handleLocalizationChange);
  }

  useOnLocalizationChange() {
    setI18nConfig();
    this.forceUpdate();
  }

  selectItem(item, index) {
    const { selectItem } = this.props;
    selectItem(item.id, index);
  }

  processBarcode(barcodes) {
    if (barcodes.length > 0) {
      const barcode = barcodes[0];
      const { setFilters } = this.props;

      // this.props.resetFilters();
      setFilters({
        barcode
      });
    } else {
      const { showErrorMessage } = this.props;
      showErrorMessage(translate('no_barcode_detected'));
    }
  }

  useOnEndReached() {
    const { itemsIsLoading, currentPage, fetchItems, sorts, filters, pageSize } = this.props;
    // increase the page
    if (itemsIsLoading === false) {
      fetchItems(currentPage + 1, sorts, filters, pageSize);
    }
  }

  useOnRefresh() {
    const { itemsIsLoading, resetItems, fetchItems, sorts, filters, pageSize } = this.props;
    // increase the page
    if (itemsIsLoading === false) {
      // fetch item first page
      resetItems();
      fetchItems(1, sorts, filters, pageSize);
    }
  }

  useOnScanBarcode() {
    const { startScanner } = this.props;
    const { scannerToken } = this.state;
    // scanner token is to identify the scan result is belong to who
    startScanner(scannerToken);
  }

  useShowFilters() {
    this.setState({ isFilterVisible: true });
  }

  renderItem({ item, index }) {
    return (
      <ListItem
        key={item.id}
        title={item.code}
        subtitle={item.desc_01}
        leftElement={
          <Image
            source={item.feat_icon_url ? { uri: item.feat_icon_url } : Images.noPhotoIcon}
            style={Style.thumbnail}
            resizeMode="contain"
            PlaceholderContent={<ActivityIndicator />}
          />
        }
        onPress={() => this.selectItem(item, index)}
        onLongPress={() => this.selectItem(item, index)}
        bottomDivider
        chevron
      />
    );
  }

  render() {
    let filterCount = 0;
    const { filters, successMessage, errorMessage, items, itemsIsLoading } = this.props;
    const { isFilterVisible } = this.state;
    Object.keys(filters).forEach(field => {
      if (Array.isArray(filters[field])) {
        if (filters[field].length > 0) {
          filterCount += 1;
        }
      } else if (filters[field]) {
        filterCount += 1;
      }
    });

    return (
      <SafeAreaView style={Style.container}>
        {successMessage !== '' && (
          <View style={Style.successContainer}>
            <Text style={Style.successText}>{successMessage}</Text>
          </View>
        )}
        {errorMessage !== '' && (
          <View style={Style.errorContainer}>
            <Text style={Style.errorText}>{errorMessage}</Text>
          </View>
        )}
        <FiltersDialog
          isVisible={isFilterVisible}
          onBackdropPress={() => this.setState({ isFilterVisible: false })}
        />
        <View style={Style.topBarContainer}>
          <View>
            <Avatar
              size="medium"
              rounded
              icon={{
                name: 'filter',
                type: 'material-community',
                size: 25
              }}
              onPress={this.useShowFilters}
            />
            {filterCount > 0 && (
              <Badge
                status="primary"
                containerStyle={{ position: 'absolute', top: -5, right: -5 }}
                value={filterCount}
              />
            )}
          </View>
          <View>
            <Button
              icon={<Icon name="barcode-scan" type="material-community" size={25} />}
              onPress={this.useOnScanBarcode}
            />
          </View>
        </View>
        <View style={Style.listContainer}>
          <FlatList
            keyExtractor={item => item.id.toString()}
            data={items}
            onEndReached={this.useOnEndReached}
            onEndReachedThreshold={0.5}
            renderItem={this.renderItem}
            refreshing={itemsIsLoading}
            onRefresh={this.useOnRefresh}
          />
        </View>
      </SafeAreaView>
    );
  }
}

BrochureScreen.propTypes = {
  sorts: PropTypes.object, // No idea how it looks like
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      code: PropTypes.string,
      ref_code_01: PropTypes.string,
      desc_01: PropTypes.string,
      desc_02: PropTypes.string,
      batch_serial_control: PropTypes.number,
      inspection_control: PropTypes.number,
      storage_class: PropTypes.number,
      item_group_01_id: PropTypes.number,
      item_group_02_id: PropTypes.number,
      item_group_03_id: PropTypes.number,
      item_group_04_id: PropTypes.number,
      item_group_05_id: PropTypes.number,
      item_group_01_code: PropTypes.string,
      item_group_02_code: PropTypes.string,
      item_group_03_code: PropTypes.string,
      item_group_04_code: PropTypes.string,
      item_group_05_code: PropTypes.string,
      s_storage_class: PropTypes.number,
      s_item_group_01_id: PropTypes.number,
      s_item_group_02_id: PropTypes.number,
      s_item_group_03_id: PropTypes.number,
      s_item_group_04_id: PropTypes.number,
      s_item_group_05_id: PropTypes.number,
      s_item_group_01_code: PropTypes.string,
      s_item_group_02_code: PropTypes.string,
      s_item_group_03_code: PropTypes.string,
      s_item_group_04_code: PropTypes.string,
      s_item_group_05_code: PropTypes.string,
      qty_scale: PropTypes.number,
      item_type: PropTypes.number,
      scan_mode: PropTypes.number,
      retrieval_method: PropTypes.number,
      unit_uom_id: PropTypes.number,
      case_uom_id: PropTypes.number,
      case_uom_rate: PropTypes.string,
      pallet_uom_rate: PropTypes.string,
      case_ext_length: PropTypes.number,
      case_ext_width: PropTypes.number,
      case_ext_height: PropTypes.number,
      case_gross_weight: PropTypes.string,
      cases_per_pallet_length: PropTypes.number,
      cases_per_pallet_width: PropTypes.number,
      no_of_layers: PropTypes.number,
      is_length_allowed_vertical: PropTypes.number,
      is_width_allowed_vertical: PropTypes.number,
      is_height_allowed_vertical: PropTypes.number,
      status: PropTypes.number,
      created_at: PropTypes.string,
      updated_at: PropTypes.string,
      str_storage_class: PropTypes.string,
      str_item_type: PropTypes.string,
      str_scan_mode: PropTypes.string,
      str_retrieval_method: PropTypes.string,
      str_status: PropTypes.string,
      feat_icon_url: PropTypes.string
    })
  ),
  filters: PropTypes.shape({
    barcode: PropTypes.string.isRequired,
    code: PropTypes.string.isRequired,
    desc: PropTypes.string.isRequired,
    item_group01_code_in: PropTypes.array
  }),
  barcodes: PropTypes.arrayOf(PropTypes.string),
  itemsIsLoading: PropTypes.bool,
  scannerToken: PropTypes.number,
  currentPage: PropTypes.number,
  pageSize: PropTypes.string,
  successMessage: PropTypes.string,
  errorMessage: PropTypes.string,
  resetItems: PropTypes.func,
  fetchItems: PropTypes.func,
  selectItem: PropTypes.func,
  setFilters: PropTypes.func,
  showErrorMessage: PropTypes.func,
  startScanner: PropTypes.func
};

BrochureScreen.defaultProps = {
  items: [],
  barcodes: [],
  sorts: {},
  itemsIsLoading: false,
  scannerToken: 0,
  currentPage: 0,
  pageSize: '',
  successMessage: '',
  errorMessage: '',
  resetItems: () => {},
  fetchItems: () => {},
  selectItem: () => {},
  setFilters: () => {},
  showErrorMessage: () => {},
  startScanner: () => {},
  filters: {
    barcode: '',
    code: '',
    desc: '',
    item_group01_code_in: []
  }
};

const mapStateToProps = state => ({
  items: state.brochure.items,
  itemsIsLoading: state.brochure.itemsIsLoading,
  sorts: state.brochure.sorts,
  filters: state.brochure.filters,
  currentPage: state.brochure.currentPage,
  pageSize: state.brochure.pageSize,
  successMessage: state.brochure.successMessage,
  errorMessage: state.brochure.errorMessage,

  scannerToken: state.barcodeScanner.token,
  barcodes: state.barcodeScanner.barcodes,
  scannerIsSuccess: state.barcodeScanner.scannerIsSuccess
});

const mapDispatchToProps = dispatch => ({
  resetItems: () => dispatch(BrochureActions.resetItems()),
  fetchItems: (page, sorts, filters, pageSize) =>
    dispatch(BrochureActions.fetchItems(page, sorts, filters, pageSize)),
  setFilters: filters => dispatch(BrochureActions.setFilters(filters)),
  resetFilters: () => dispatch(BrochureActions.resetFilters()),

  selectItem: (itemId, index) => dispatch(ItemDetailsActions.selectItem(itemId, index)),

  startScanner: token => dispatch(BarcodeScannerActions.startScanner(token)),
  showErrorMessage: errorMessage => dispatch(BarcodeScannerActions.showErrorMessage(errorMessage))
});

export default connect(mapStateToProps, mapDispatchToProps)(BrochureScreen);
