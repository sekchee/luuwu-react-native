import React from 'react';
import { ScrollView, View } from 'react-native';
import { Button, Overlay, Input, Divider, Text, Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { SafeAreaView } from 'react-navigation';
import Style from './FiltersStyle';
import {
  translate,
  setI18nConfig,
  addLocaleChangedListener,
  removeLocaleChangedListener
} from '../../Translations';
import { MultiSelectButton } from '../../Components/MultiSelectButton';
import BrochureActions from '../../Stores/Brochure/Actions';
// import BarcodeScannerActions from '../../Stores/BarcodeScanner/Actions';

class FiltersDialog extends React.PureComponent {
  constructor() {
    super();

    // this.state = {
    //   scannerToken: Date.now()
    // };

    setI18nConfig(); // set initial config

    this.useOnLocalizationChange = this.useOnLocalizationChange.bind(this);
    // this.useOnScanBarcode = this.useOnScanBarcode.bind(this);
  }

  componentDidMount() {
    addLocaleChangedListener(this.handleLocalizationChange);

    const { resetItemGroup01s, fetchAllItemGroup01s } = this.props;
    resetItemGroup01s();
    fetchAllItemGroup01s();
  }

  componentDidUpdate() {}

  componentWillUnmount() {
    removeLocaleChangedListener(this.handleLocalizationChange);
  }

  useOnLocalizationChange() {
    setI18nConfig();
    this.forceUpdate();
  }

  render() {
    const {
      itemGroup01s,
      isVisible,
      onBackdropPress,
      filters,
      resetFilters,
      setFilters,
      useOnScanBarcode
    } = this.props;
    const itemGroup01SelectItems = itemGroup01s.map(itemGroup01 => {
      return {
        value: itemGroup01.code,
        label: itemGroup01.desc_01
      };
    });

    return (
      <Overlay
        fullScreen
        isVisible={isVisible}
        onBackdropPress={onBackdropPress}
        useOnScanBarcode={useOnScanBarcode}
      >
        <SafeAreaView style={Style.safeView}>
          <View style={Style.topBarContainer}>
            <View>
              <Button
                icon={<Icon name="arrow-left" type="material-community" size={25} />}
                onPress={() => {
                  onBackdropPress();
                }}
              />
            </View>
            <View>
              <Button
                icon={<Icon name="barcode-scan" type="material-community" size={25} />}
                onPress={() => {
                  useOnScanBarcode();
                  // onBackdropPress();
                }}
              />
            </View>
          </View>
          <View style={Style.container}>
            <Formik
              enableReinitialize
              initialValues={filters}
              onReset={() => {
                resetFilters();
              }}
              onSubmit={values => {
                setFilters(values);
                onBackdropPress();
              }}
              validationSchema={Yup.object().shape({})}
            >
              {({
                values,
                handleChange,
                errors,
                isSubmitting,
                isValid,
                handleSubmit,
                handleReset
              }) => (
                <ScrollView>
                  <Text h4 style={Style.title}>
                    {translate('details')}
                  </Text>
                  <Input
                    containerStyle={Style.formElement}
                    value={values.barcode}
                    onChangeText={handleChange('barcode')}
                    placeholder={translate('barcode')}
                    errorMessage={errors.barcode}
                    disabled={isSubmitting}
                  />
                  <Input
                    containerStyle={Style.formElement}
                    value={values.code}
                    onChangeText={handleChange('code')}
                    placeholder={translate('code')}
                    errorMessage={errors.code}
                    disabled={isSubmitting}
                  />
                  <Input
                    containerStyle={Style.formElement}
                    value={values.desc}
                    onChangeText={handleChange('desc')}
                    placeholder={translate('description')}
                    errorMessage={errors.desc}
                    disabled={isSubmitting}
                  />
                  <Divider style={Style.divider} />
                  <Text h4 style={Style.title}>
                    {translate('brand')}
                  </Text>
                  <MultiSelectButton
                    containerStyle={Style.selectButtonContainer}
                    selectedValue={values.item_group01_code_in}
                    onValueChange={handleChange('item_group01_code_in')}
                    items={itemGroup01SelectItems}
                    errorMessage={errors.item_group01_code_in}
                    disabled={isSubmitting}
                  />
                  <Divider style={Style.divider} />
                  <View style={Style.filterButtonsContainer}>
                    <Button
                      containerStyle={{ ...Style.formElement, flex: 0.5 }}
                      loading={isSubmitting}
                      onPress={handleReset}
                      title={translate('reset')}
                    />
                    <Button
                      containerStyle={{ ...Style.formElement, flex: 0.5 }}
                      disabled={!isValid}
                      loading={isSubmitting}
                      onPress={handleSubmit}
                      title={translate('done')}
                    />
                  </View>
                </ScrollView>
              )}
            </Formik>
          </View>
        </SafeAreaView>
      </Overlay>
    );
  }
}

FiltersDialog.propTypes = {
  filters: PropTypes.shape({
    barcode: PropTypes.string.isRequired,
    code: PropTypes.string.isRequired,
    desc: PropTypes.string.isRequired,
    item_group01_code_in: PropTypes.array
  }),

  isVisible: PropTypes.bool,
  itemGroup01s: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      code: PropTypes.string,
      desc_01: PropTypes.string,
      desc_02: PropTypes.string,
      created_at: PropTypes.string,
      updated_at: PropTypes.string
    })
  ),

  resetItemGroup01s: PropTypes.func,
  fetchAllItemGroup01s: PropTypes.func,
  onBackdropPress: PropTypes.func,
  setFilters: PropTypes.func,
  resetFilters: PropTypes.func,
  // startScanner: PropTypes.func,
  useOnScanBarcode: PropTypes.func
};

FiltersDialog.defaultProps = {
  filters: {
    barcode: '',
    code: '',
    desc: '',
    item_group01_code_in: []
  },

  itemGroup01s: [],
  isVisible: false,
  resetItemGroup01s: () => {},
  fetchAllItemGroup01s: () => {},
  onBackdropPress: () => {},
  setFilters: () => {},
  resetFilters: () => {},
  // startScanner: () => {},
  useOnScanBarcode: () => {}
};

const mapStateToProps = state => ({
  filters: state.brochure.filters,
  itemGroup01s: state.brochure.itemGroup01s
  // scannerToken: state.barcodeScanner.token
});

const mapDispatchToProps = dispatch => ({
  resetItemGroup01s: () => dispatch(BrochureActions.resetItemGroup01s()),
  fetchAllItemGroup01s: () => dispatch(BrochureActions.fetchAllItemGroup01s()),
  setFilters: filters => dispatch(BrochureActions.setFilters(filters)),
  resetFilters: () => dispatch(BrochureActions.resetFilters())
  // startScanner: token => dispatch(BarcodeScannerActions.startScanner(token))
});

export default connect(mapStateToProps, mapDispatchToProps)(FiltersDialog);
