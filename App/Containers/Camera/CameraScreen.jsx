import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Slider from '@react-native-community/slider';
import { RNCamera } from 'react-native-camera';
import Style from './CameraStyle';

const flashModeOrder = {
  off: 'on',
  on: 'auto',
  auto: 'torch',
  torch: 'off'
};

const wbOrder = {
  auto: 'sunny',
  sunny: 'cloudy',
  cloudy: 'shadow',
  shadow: 'fluorescent',
  fluorescent: 'incandescent',
  incandescent: 'auto'
};

const landmarkSize = 2;

export default class CameraScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      flash: 'off',
      zoom: 0,
      autoFocus: 'on',
      depth: 0,
      type: 'back',
      whiteBalance: 'auto',
      ratio: '16:9',
      recordOptions: {
        mute: false,
        maxDuration: 5,
        quality: RNCamera.Constants.VideoQuality['288p']
      },
      isRecording: false,
      canDetectFaces: false,
      canDetectText: false,
      canDetectBarcode: false,
      faces: [],
      textBlocks: [],
      barcodes: []
    };

    this.toggleFacing = this.toggleFacing.bind(this);
    this.toggleFlash = this.toggleFlash.bind(this);
    this.toggleFocus = this.toggleFocus.bind(this);
    this.toggleWB = this.toggleWB.bind(this);
    this.zoomIn = this.zoomIn.bind(this);
    this.zoomOut = this.zoomOut.bind(this);
    this.setFocusDepth = this.setFocusDepth.bind(this);
    this.toggle = this.toggle.bind(this);
    this.takeVideo = this.takeVideo.bind(this);
    this.takePicture = this.takePicture.bind(this);
  }

  componentWillUnmount() {
    // console.log('CameraScreen.componentWillUnmount');
  }

  setFocusDepth(depth) {
    this.setState({
      depth
    });
  }

  toggleFacing() {
    this.setState(prevState => ({ type: prevState.type === 'back' ? 'front' : 'back' }));
  }

  toggleFlash() {
    this.setState(prevState => ({
      flash: flashModeOrder[prevState.flash]
    }));
  }

  toggleWB() {
    this.setState(prevState => ({
      whiteBalance: wbOrder[prevState.whiteBalance]
    }));
  }

  toggleFocus() {
    this.setState(prevState => ({
      autoFocus: prevState.autoFocus === 'on' ? 'off' : 'on'
    }));
  }

  zoomOut() {
    this.setState(prevState => ({
      zoom: prevState.zoom - 0.1 < 0 ? 0 : prevState.zoom - 0.1
    }));
  }

  zoomIn() {
    this.setState(prevState => ({
      zoom: prevState.zoom + 0.1 > 1 ? 1 : prevState.zoom + 0.1
    }));
  }

  async takePicture() {
    if (this.camera) {
      // const data = await this.camera.takePictureAsync();
      // console.warn('takePicture ', data);
    }
  }

  async takeVideo() {
    if (this.camera) {
      try {
        const { recordOptions } = this.state;
        const promise = this.camera.recordAsync(recordOptions);

        if (promise) {
          this.setState({ isRecording: true });
          // const data = await promise;
          this.setState({ isRecording: false });
          // console.warn('takeVideo', data);
        }
      } catch (e) {
        // console.error(e);
      }
    }
  }

  toggle(value) {
    return () => {
      this.setState(prevState => ({ [value]: !prevState[value] }));
    };
  }

  facesDetected({ faces }) {
    this.setState({ faces });
  }

  textRecognized(object) {
    const { textBlocks } = object;
    this.setState({ textBlocks });
  }

  barcodeRecognized({ barcodes }) {
    this.setState({ barcodes });
  }

  static renderFace({ bounds, faceID, rollAngle, yawAngle }) {
    return (
      <View
        key={faceID}
        transform={[
          { perspective: 600 },
          { rotateZ: `${rollAngle.toFixed(0)}deg` },
          { rotateY: `${yawAngle.toFixed(0)}deg` }
        ]}
        style={[
          Style.face,
          {
            ...bounds.size,
            left: bounds.origin.x,
            top: bounds.origin.y
          }
        ]}
      >
        <Text style={Style.faceText}>ID: {faceID}</Text>
        <Text style={Style.faceText}>rollAngle: {rollAngle.toFixed(0)}</Text>
        <Text style={Style.faceText}>yawAngle: {yawAngle.toFixed(0)}</Text>
      </View>
    );
  }

  static renderLandmarksOfFace(face) {
    const renderLandmark = position =>
      position && (
        <View
          style={[
            Style.landmark,
            {
              left: position.x - landmarkSize / 2,
              top: position.y - landmarkSize / 2
            }
          ]}
        />
      );
    return (
      <View key={`landmarks-${face.faceID}`}>
        {renderLandmark(face.leftEyePosition)}
        {renderLandmark(face.rightEyePosition)}
        {renderLandmark(face.leftEarPosition)}
        {renderLandmark(face.rightEarPosition)}
        {renderLandmark(face.leftCheekPosition)}
        {renderLandmark(face.rightCheekPosition)}
        {renderLandmark(face.leftMouthPosition)}
        {renderLandmark(face.mouthPosition)}
        {renderLandmark(face.rightMouthPosition)}
        {renderLandmark(face.noseBasePosition)}
        {renderLandmark(face.bottomMouthPosition)}
      </View>
    );
  }

  renderFaces() {
    const { faces } = this.state;
    return (
      <View style={Style.facesContainer} pointerEvents="none">
        {faces.map(CameraScreen.renderFace)}
      </View>
    );
  }

  renderLandmarks() {
    const { faces } = this.state;
    return (
      <View style={Style.facesContainer} pointerEvents="none">
        {faces.map(CameraScreen.renderLandmarksOfFace)}
      </View>
    );
  }

  renderTextBlocks() {
    const { textBlocks } = this.state;
    return (
      <View style={Style.facesContainer} pointerEvents="none">
        {textBlocks.map(CameraScreen.renderTextBlock)}
      </View>
    );
  }

  static renderTextBlock({ bounds, value }) {
    return (
      <React.Fragment key={value + bounds.origin.x}>
        <Text style={[Style.textBlock, { left: bounds.origin.x, top: bounds.origin.y }]}>
          {value}
        </Text>
        <View
          style={[
            Style.text,
            {
              ...bounds.size,
              left: bounds.origin.x,
              top: bounds.origin.y
            }
          ]}
        />
      </React.Fragment>
    );
  }

  renderBarcodes() {
    const { barcodes } = this.state;
    return (
      <View style={Style.facesContainer} pointerEvents="none">
        {barcodes.map(CameraScreen.renderBarcode)}
      </View>
    );
  }

  static renderBarcode({ bounds, data, type }) {
    return (
      <React.Fragment key={data + bounds.origin.x}>
        <View
          style={[
            Style.text,
            {
              ...bounds.size,
              left: bounds.origin.x,
              top: bounds.origin.y
            }
          ]}
        >
          <Text style={[Style.textBlock]}>{`${data} ${type}`}</Text>
        </View>
      </React.Fragment>
    );
  }

  renderCamera() {
    const {
      canDetectFaces,
      canDetectText,
      canDetectBarcode,
      type,
      flash,
      autoFocus,
      zoom,
      whiteBalance,
      ratio,
      depth,
      isRecording
    } = this.state;
    return (
      <RNCamera
        ref={ref => {
          this.camera = ref;
        }}
        style={{
          flex: 1
        }}
        type={type}
        flashMode={flash}
        autoFocus={autoFocus}
        zoom={zoom}
        whiteBalance={whiteBalance}
        ratio={ratio}
        focusDepth={depth}
        trackingEnabled
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel'
        }}
        faceDetectionLandmarks={
          RNCamera.Constants.FaceDetection.Landmarks
            ? RNCamera.Constants.FaceDetection.Landmarks.all
            : undefined
        }
        faceDetectionClassifications={
          RNCamera.Constants.FaceDetection.Classifications
            ? RNCamera.Constants.FaceDetection.Classifications.all
            : undefined
        }
        onFacesDetected={canDetectFaces ? this.facesDetected : null}
        onTextRecognized={canDetectText ? this.textRecognized : null}
        onGoogleVisionBarcodesDetected={canDetectBarcode ? this.barcodeRecognized : null}
        googleVisionBarcodeType={RNCamera.Constants.GoogleVisionBarcodeDetection.BarcodeType.ALL}
      >
        <View
          style={{
            flex: 0.5
          }}
        >
          <View
            style={{
              backgroundColor: 'transparent',
              flexDirection: 'row',
              justifyContent: 'space-around'
            }}
          >
            <TouchableOpacity style={Style.flipButton} onPress={this.toggleFacing}>
              <Text style={Style.flipText}> FLIP </Text>
            </TouchableOpacity>
            <TouchableOpacity style={Style.flipButton} onPress={this.toggleFlash}>
              <Text style={Style.flipText}> FLASH: {flash} </Text>
            </TouchableOpacity>
            <TouchableOpacity style={Style.flipButton} onPress={this.toggleWB}>
              <Text style={Style.flipText}> WB: {whiteBalance} </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              backgroundColor: 'transparent',
              flexDirection: 'row',
              justifyContent: 'space-around'
            }}
          >
            <TouchableOpacity onPress={this.toggle('canDetectFaces')} style={Style.flipButton}>
              <Text style={Style.flipText}>
                {!canDetectFaces ? 'Detect Faces' : 'Detecting Faces'}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.toggle('canDetectText')} style={Style.flipButton}>
              <Text style={Style.flipText}>
                {!canDetectText ? 'Detect Text' : 'Detecting Text'}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.toggle('canDetectBarcode')} style={Style.flipButton}>
              <Text style={Style.flipText}>
                {!canDetectBarcode ? 'Detect Barcode' : 'Detecting Barcode'}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            flex: 0.4,
            backgroundColor: 'transparent',
            flexDirection: 'row',
            alignSelf: 'flex-end'
          }}
        >
          <Slider
            style={{ width: 150, marginTop: 15, alignSelf: 'flex-end' }}
            onValueChange={this.setFocusDepth}
            step={0.1}
            disabled={autoFocus === 'on'}
          />
        </View>
        <View
          style={{
            flex: 0.1,
            backgroundColor: 'transparent',
            flexDirection: 'row',
            alignSelf: 'flex-end'
          }}
        >
          <TouchableOpacity
            style={[
              Style.flipButton,
              {
                flex: 0.3,
                alignSelf: 'flex-end',
                backgroundColor: isRecording ? 'white' : 'darkred'
              }
            ]}
            onPress={isRecording ? () => {} : this.takeVideo}
          >
            {isRecording ? (
              // The following rule is disabled because it applies to the web based react and not react native
              // eslint-disable-next-line jsx-a11y/accessible-emoji
              <Text style={Style.flipText}> ☕ </Text>
            ) : (
              <Text style={Style.flipText}> REC </Text>
            )}
          </TouchableOpacity>
        </View>
        {zoom !== 0 && <Text style={[Style.flipText, Style.zoomText]}>Zoom: {zoom}</Text>}
        <View
          style={{
            flex: 0.1,
            backgroundColor: 'transparent',
            flexDirection: 'row',
            alignSelf: 'flex-end'
          }}
        >
          <TouchableOpacity
            style={[Style.flipButton, { flex: 0.1, alignSelf: 'flex-end' }]}
            onPress={this.zoomIn}
          >
            <Text style={Style.flipText}> + </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[Style.flipButton, { flex: 0.1, alignSelf: 'flex-end' }]}
            onPress={this.zoomOut}
          >
            <Text style={Style.flipText}> - </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[Style.flipButton, { flex: 0.25, alignSelf: 'flex-end' }]}
            onPress={this.toggleFocus}
          >
            <Text style={Style.flipText}> AF : {autoFocus} </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[Style.flipButton, Style.picButton, { flex: 0.3, alignSelf: 'flex-end' }]}
            onPress={this.takePicture}
          >
            <Text style={Style.flipText}> SNAP </Text>
          </TouchableOpacity>
        </View>
        {!!canDetectFaces && this.renderFaces()}
        {!!canDetectFaces && this.renderLandmarks()}
        {!!canDetectText && this.renderTextBlocks()}
        {!!canDetectBarcode && this.renderBarcodes()}
      </RNCamera>
    );
  }

  render() {
    return <View style={Style.container}>{this.renderCamera()}</View>;
  }
}
