import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  itemContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  topBarContainer: {
    flex: 0.1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10
  },
  listContainer: {
    flex: 0.9,
    flexDirection: 'column',
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10
  },
  thumbnail: {
    width: 150,
    height: 200,
    alignSelf: 'center'
  },
  formContainer: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10
  },
  formElement: {
    marginTop: 5
  },
  filterButtonsContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  errorContainer: {
    flex: 0.1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFB6C1'
  },
  errorText: {
    marginLeft: 10,
    color: 'red'
  },
  successContainer: {
    flex: 0.1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#98FB98'
  },
  successText: {
    marginLeft: 10,
    color: 'green'
  },
  title: {
    color: 'grey',
    fontSize: 20,
    fontWeight: 'bold'
  },
  subtitle: {
    fontSize: 10
  }
});
