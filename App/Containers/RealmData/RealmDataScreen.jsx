import React from 'react';
import { View, FlatList, Picker } from 'react-native';
import { Button, ListItem, Icon, Overlay, Input, Text, SearchBar } from 'react-native-elements';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import { PropTypes } from 'prop-types';
import ApiService from '../../Services/ApiService';
import AppActions from '../../Stores/App/Actions';

const Realm = require('realm');

const ItemSchema = {
  name: 'Item',
  primaryKey: 'id',
  properties: {
    id: 'int',
    code: 'string',
    ref_code_01: 'string',
    desc_01: 'string',
    desc_02: 'string',
    batch_serial_control: 'int',
    inspection_control: 'int',
    storage_class: 'int',
    item_group_01_id: 'int',
    item_group_02_id: 'int',
    item_group_03_id: 'int',
    item_group_04_id: 'int',
    item_group_05_id: 'int',
    item_group_01_code: 'string',
    item_group_02_code: 'string',
    item_group_03_code: 'string',
    item_group_04_code: 'string',
    item_group_05_code: 'string',
    s_storage_class: 'int',
    s_item_group_01_id: 'int',
    s_item_group_02_id: 'int',
    s_item_group_03_id: 'int',
    s_item_group_04_id: 'int',
    s_item_group_05_id: 'int',
    s_item_group_01_code: 'string',
    s_item_group_02_code: 'string',
    s_item_group_03_code: 'string',
    s_item_group_04_code: 'string',
    s_item_group_05_code: 'string',
    qty_scale: 'int',
    item_type: 'int',
    scan_mode: 'int',
    retrieval_method: 'int',
    unit_uom_id: 'int',
    case_uom_id: 'int',
    case_uom_rate: 'string',
    pallet_uom_rate: 'string',
    case_ext_length: 'int',
    case_ext_width: 'int',
    case_ext_height: 'int',
    case_gross_weight: 'string',
    cases_per_pallet_length: 'int',
    cases_per_pallet_width: 'int',
    no_of_layers: 'int',
    is_length_allowed_vertical: 'int',
    is_width_allowed_vertical: 'int',
    is_height_allowed_vertical: 'int',
    status: 'int',
    created_at: 'string',
    updated_at: 'string',
    record_id: 'int',
    first_synced_at: 'string',
    last_synced_at: 'string'
  }
};

class RealmDataScreen extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      realm: null,
      refreshList: false,
      initialGet: false,
      selectedItemId: null,
      showOverlay: false,
      search: '',
      sortType: 'code',
      sortDirection: false // False is ascending, True is descending
    };

    this.renderItem = this.renderItem.bind(this);
    this.updateSearch = this.updateSearch.bind(this);
  }

  componentDidMount() {
    const { search, sortType, sortDirection } = this.state;
    Realm.open({
      schema: [ItemSchema]
    }).then(realm => {
      this.setState({
        realm,
        initialGet: realm.objects('Item').length !== 0
      });
      realm
        .objects('Item')
        .filtered('code CONTAINS $0', search)
        .sorted(sortType, sortDirection)
        .addListener(() => {
          this.setState(prevState => ({
            refreshList: !prevState.refreshList
          }));
        });
    });
  }

  componentWillUnmount() {
    // Close the realm if there is one open.
    const { realm } = this.state;
    if (realm !== null && !realm.isClosed) {
      realm.close();
    }
  }

  onItemPressed(item) {
    this.setState({
      selectedItemId: item.id,
      showOverlay: true
    });
  }

  async getItems(pageSize) {
    try {
      const { app } = this.props;
      const getData = {
        pageSize
      };

      const result = await ApiService.getApi(
        app.apiUrl,
        `mobile/indexData/12/items`,
        app.token,
        getData
      );

      if (result.isSuccess === true) {
        const { realm } = this.state;
        realm.write(() => {
          realm.deleteAll();
          result.data.data.forEach(item => {
            realm.create('Item', item);
          });
        });
      } else if (result.isTokenExpired === true) {
        AppActions.tokenExpired(result.message);
      } else if (result.isPasswordExpired === true) {
        AppActions.passwordExpired(result.message);
      } else {
        // console.log(result.message);
      }
    } catch (error) {
      // console.log(error);
    }
  }

  deleteItem(item) {
    const { realm } = this.state;
    try {
      realm.write(() => {
        realm.delete(item);
      });
    } catch (error) {
      // console.log(error);
    }
  }

  deleteAllItems() {
    const { realm } = this.state;
    try {
      realm.write(() => {
        realm.deleteAll();
      });
    } catch (error) {
      // console.log(error);
    }
  }

  updateSearch(searchText) {
    this.setState({
      search: searchText
    });
  }

  renderItem({ item }) {
    return (
      <ListItem
        style={{ flex: 1 }}
        title={item.code}
        subtitle={item.id.toString()}
        onPress={() => {
          this.onItemPressed(item);
        }}
        rightIcon={
          <Icon
            name="alpha-x-circle"
            type="material-community"
            color="#FF5733"
            size={32}
            onPress={() => this.deleteItem(item)}
          />
        }
        bottomDivider
      />
    );
  }

  render() {
    const {
      realm,
      refreshList,
      initialGet,
      showOverlay,
      selectedItemId,
      search,
      sortType,
      sortDirection
    } = this.state;
    let selectedItem;
    if (selectedItemId != null) {
      selectedItem = realm.objectForPrimaryKey('Item', selectedItemId);
    }
    return (
      <View style={{ flex: 1 }}>
        <Button
          onPress={() => {
            this.setState({
              initialGet: true
            });
            this.getItems(1000);
          }}
          title={initialGet ? 'Reset Item List' : 'Get Items'}
        />
        <Button
          onPress={() => {
            this.deleteAllItems();
          }}
          title="Delete All Items"
        />
        <SearchBar
          lightTheme
          inputContainerStyle={{ backgroundColor: 'white' }}
          placeholder="Type here to filter"
          onChangeText={this.updateSearch}
          value={search}
        />
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 0.6 }}>
            <Picker
              mode="dropdown"
              selectedValue={sortType}
              onValueChange={value => {
                this.setState({ sortType: value });
              }}
            >
              <Picker.Item color="#2089dc" label="Item Code" value="code" />
              <Picker.Item color="#2089dc" label="Item ID" value="id" />
            </Picker>
          </View>
          <View style={{ flex: 0.4 }}>
            <Picker
              mode="dropdown"
              selectedValue={sortDirection}
              onValueChange={value => {
                this.setState({ sortDirection: value });
              }}
            >
              <Picker.Item color="#2089dc" label="Ascending" value={false} />
              <Picker.Item color="#2089dc" label="Descending" value />
            </Picker>
          </View>
        </View>
        {realm !== null ? (
          <FlatList
            keyExtractor={item => item.id.toString()}
            data={realm
              .objects('Item')
              .filtered('code CONTAINS $0', search)
              .sorted(sortType, sortDirection)}
            renderItem={this.renderItem}
            extraData={refreshList}
          />
        ) : (
          <Text>Press the button to populate the screen.</Text>
        )}
        <Overlay
          isVisible={showOverlay}
          windowBackgroundColor="rgba(255, 255, 255, .5)"
          onBackdropPress={() => this.setState({ showOverlay: false, selectedItemId: null })}
        >
          <View>
            <Text h3>
              Edit details for item with id: {selectedItemId !== null ? selectedItemId : ''}
            </Text>
            <Formik
              enableReinitialize
              initialValues={{
                code: selectedItem !== undefined ? selectedItem.code : '',
                desc: selectedItem !== undefined ? selectedItem.desc_01 : ''
              }}
              onSubmit={values => {
                this.setState({
                  showOverlay: false,
                  selectedItemId: null
                });
                try {
                  realm.write(() => {
                    selectedItem.code = values.code;
                    selectedItem.desc_01 = values.desc;
                  });
                } catch (error) {
                  // console.log(error);
                }
              }}
            >
              {({ values, handleChange, handleSubmit, isSubmitting }) => (
                <View>
                  <Input
                    label="Item Code"
                    value={values.code}
                    onChangeText={handleChange('code')}
                  />
                  <Input
                    label="Item Description 01"
                    value={values.desc}
                    onChangeText={handleChange('desc')}
                    multiline
                    textAlignVertical="top"
                  />
                  <View style={{ marginTop: 20 }}>
                    <Button loading={isSubmitting} onPress={handleSubmit} title="Save" />
                  </View>
                </View>
              )}
            </Formik>
          </View>
        </Overlay>
      </View>
    );
  }
}

RealmDataScreen.propTypes = {
  app: PropTypes.shape({
    username: PropTypes.string,
    token: PropTypes.string,
    apiUrl: PropTypes.string,
    user: PropTypes.shape({
      username: PropTypes.string,
      email: PropTypes.string,
      first_name: PropTypes.string,
      last_name: PropTypes.string,
      timezone: PropTypes.string,
      last_login: PropTypes.string,
      password_changed_at: PropTypes.string
    }),
    successMessage: PropTypes.string,
    errorMessage: PropTypes.string
  })
};

RealmDataScreen.defaultProps = {
  app: PropTypes.shape({
    username: '',
    token: '',
    apiUrl: '',
    user: PropTypes.shape({
      username: '',
      email: '',
      first_name: '',
      last_name: '',
      timezone: '',
      last_login: '',
      password_changed_at: ''
    }),
    successMessage: '',
    errorMessage: ''
  })
};

const mapStateToProps = state => ({
  app: state.app
});

const mapDispatchToProps = dispatch => ({
  tokenExpired: message => dispatch(AppActions.tokenExpired(message)),
  passwordExpired: message => dispatch(AppActions.passwordExpired(message))
});

export default connect(mapStateToProps, mapDispatchToProps)(RealmDataScreen);
