import React from 'react';
import { SafeAreaView, View, FlatList, Picker } from 'react-native';
import { Button, ListItem, Icon, Overlay, Input, Text, SearchBar } from 'react-native-elements';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import { PropTypes } from 'prop-types';
import SQLite from 'react-native-sqlite-storage';
import ApiService from '../../Services/ApiService';
import AppActions from '../../Stores/App/Actions';
import Style from './SQLiteDataStyle';

const database = SQLite.openDatabase(
  { name: 'luuwu.db', location: 'default' },
  () => {
    // console.log('Successfully connected');
  },
  () => {
    // console.log(error);
  }
);

class SQLiteDataScreen extends React.PureComponent {
  constructor(props) {
    super(props);

    database.transaction(transaction => {
      transaction.executeSql(
        'CREATE TABLE IF NOT EXISTS `items` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,`code` varchar(35) NOT NULL,`ref_code_01` varchar(35) NOT NULL,`desc_01` varchar(191) NOT NULL,`desc_02` varchar(191) NOT NULL,`batch_serial_control` smallint(5) NOT NULL,`inspection_control` smallint(5) NOT NULL,`storage_class` smallint(5) NOT NULL,`item_group_01_id` int(10) NOT NULL,`item_group_02_id` int(10) NOT NULL,`item_group_03_id` int(10) NOT NULL,`item_group_04_id` int(10) NOT NULL,`item_group_05_id` int(10) NOT NULL,`item_group_01_code` varchar(191) NOT NULL,`item_group_02_code` varchar(191) NOT NULL,`item_group_03_code` varchar(191) NOT NULL,`item_group_04_code` varchar(191) NOT NULL,`item_group_05_code` varchar(191) NOT NULL,`s_storage_class` smallint(5) NOT NULL,`s_item_group_01_id` int(10) NOT NULL,`s_item_group_02_id` int(10) NOT NULL,`s_item_group_03_id` int(10) NOT NULL,`s_item_group_04_id` int(10) NOT NULL,`s_item_group_05_id` int(10) NOT NULL,`s_item_group_01_code` varchar(191) NOT NULL,`s_item_group_02_code` varchar(191) NOT NULL,`s_item_group_03_code` varchar(191) NOT NULL,`s_item_group_04_code` varchar(191) NOT NULL,`s_item_group_05_code` varchar(191) NOT NULL,`qty_scale` smallint(5) NOT NULL,`item_type` smallint(5) NOT NULL,`scan_mode` smallint(5) NOT NULL,`retrieval_method` smallint(5) NOT NULL,`unit_uom_id` int(10) NOT NULL,`case_uom_id` int(10) NOT NULL,`case_uom_rate` decimal(14,6) NOT NULL,`pallet_uom_rate` decimal(14,6) NOT NULL,`case_ext_length` int(10) NOT NULL,`case_ext_width` int(10) NOT NULL,`case_ext_height` int(10) NOT NULL,`case_gross_weight` decimal(25,4) NOT NULL,`cases_per_pallet_length` smallint(5) NOT NULL,`cases_per_pallet_width` smallint(5) NOT NULL,`no_of_layers` smallint(5) NOT NULL,`is_length_allowed_vertical` tinyint(1) NOT NULL,`is_width_allowed_vertical` tinyint(1) NOT NULL,`is_height_allowed_vertical` tinyint(1) NOT NULL,`status` smallint(5) NOT NULL,`created_at` timestamp NULL DEFAULT NULL,`updated_at` timestamp NULL DEFAULT NULL);',
        [],
        () => {
          // console.log('Database created');
        },
        () => {
          // console.log(error);
        }
      );
    });

    this.state = {
      initialGet: false,
      selectedItemId: null,
      showOverlay: false,
      search: '',
      sortType: 'code',
      sortDirection: 'ASC',
      items: []
    };

    this.renderItem = this.renderItem.bind(this);
    this.updateSearch = this.updateSearch.bind(this);
    this.loadItems = this.loadItems.bind(this);
    this.getItems = this.getItems.bind(this);
  }

  componentDidMount() {
    database.transaction(transaction => {
      transaction.executeSql(
        'SELECT * FROM items;',
        [],
        (anotherTransaction, result) => {
          this.setState({
            initialGet: result.rows.length !== 0
          });
          if (result.rows.length !== 0) {
            this.loadItems();
          }
        },
        () => {
          // console.log(error);
        }
      );
    });
  }

  componentWillUnmount() {
    if (database) {
      database.close();
    }
  }

  onItemPressed(item) {
    this.setState({
      selectedItemId: item.id,
      showOverlay: true
    });
  }

  async getItems(pageSize) {
    try {
      const { app } = this.props;
      const getData = {
        pageSize
      };

      const result = await ApiService.getApi(
        app.apiUrl,
        `mobile/indexData/12/items`,
        app.token,
        getData
      );

      if (result.isSuccess === true) {
        database.transaction(transaction => {
          transaction.executeSql('DELETE FROM items', [], () => {
            const query =
              'INSERT INTO `items` (`id`, `code`, `ref_code_01`, `desc_01`, `desc_02`, `batch_serial_control`, `inspection_control`, `storage_class`, `item_group_01_id`, `item_group_02_id`, `item_group_03_id`, `item_group_04_id`, `item_group_05_id`, `item_group_01_code`, `item_group_02_code`, `item_group_03_code`, `item_group_04_code`, `item_group_05_code`, `s_storage_class`, `s_item_group_01_id`, `s_item_group_02_id`, `s_item_group_03_id`, `s_item_group_04_id`, `s_item_group_05_id`, `s_item_group_01_code`, `s_item_group_02_code`, `s_item_group_03_code`, `s_item_group_04_code`, `s_item_group_05_code`, `qty_scale`, `item_type`, `scan_mode`, `retrieval_method`, `unit_uom_id`, `case_uom_id`, `case_uom_rate`, `pallet_uom_rate`, `case_ext_length`, `case_ext_width`, `case_ext_height`, `case_gross_weight`, `cases_per_pallet_length`, `cases_per_pallet_width`, `no_of_layers`, `is_length_allowed_vertical`, `is_width_allowed_vertical`, `is_height_allowed_vertical`, `status`, `created_at`, `updated_at`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
            result.data.data.forEach(item => {
              transaction.executeSql(
                query,
                [
                  item.id,
                  item.code,
                  item.ref_code_01,
                  item.desc_01,
                  item.desc_02,
                  item.batch_serial_control,
                  item.inspection_control,
                  item.storage_class,
                  item.item_group_01_id,
                  item.item_group_02_id,
                  item.item_group_03_id,
                  item.item_group_04_id,
                  item.item_group_05_id,
                  item.item_group_01_code,
                  item.item_group_02_code,
                  item.item_group_03_code,
                  item.item_group_04_code,
                  item.item_group_05_code,
                  item.s_storage_class,
                  item.s_item_group_01_id,
                  item.s_item_group_02_id,
                  item.s_item_group_03_id,
                  item.s_item_group_04_id,
                  item.s_item_group_05_id,
                  item.s_item_group_01_code,
                  item.s_item_group_02_code,
                  item.s_item_group_03_code,
                  item.s_item_group_04_code,
                  item.s_item_group_05_code,
                  item.qty_scale,
                  item.item_type,
                  item.scan_mode,
                  item.retrieval_method,
                  item.unit_uom_id,
                  item.case_uom_id,
                  item.case_uom_rate,
                  item.pallet_uom_rate,
                  item.case_ext_length,
                  item.case_ext_width,
                  item.case_ext_height,
                  item.case_gross_weight,
                  item.cases_per_pallet_length,
                  item.cases_per_pallet_width,
                  item.no_of_layers,
                  item.is_length_allowed_vertical,
                  item.is_width_allowed_vertical,
                  item.is_height_allowed_vertical,
                  item.status,
                  item.created_at,
                  item.updated_at
                ],
                () => {
                  // console.log('Success');
                },
                () => {
                  // console.log(error);
                }
              );
            });
            this.loadItems();
          });
        });
      } else if (result.isTokenExpired === true) {
        AppActions.tokenExpired(result.message);
      } else if (result.isPasswordExpired === true) {
        AppActions.passwordExpired(result.message);
      } else {
        // console.log(result.message);
      }
    } catch (error) {
      // console.log(error);
    }
  }

  async loadItems() {
    const { search, sortType, sortDirection } = this.state;
    database.transaction(transaction => {
      const items = new Map();
      transaction.executeSql(
        `SELECT * FROM items WHERE code LIKE '%${search}%' ORDER BY ${sortType} ${sortDirection};`,
        [],
        (anotherTransaction, result) => {
          for (let i = 0; i < result.rows.length; i += 1) {
            const item = result.rows.item(i);
            items.set(item.id, item);
          }
          this.setState({
            items
          });
        },
        () => {
          // console.log(error);
        }
      );
    });
  }

  deleteItem(deletedItem) {
    const { items } = this.state;
    const itemID = deletedItem.id;
    database.transaction(transaction => {
      transaction.executeSql(
        'DELETE FROM items WHERE id=?;',
        [itemID],
        () => {
          const itemsCopy = new Map(items);
          itemsCopy.delete(itemID);
          this.setState({
            items: itemsCopy
          });
        },
        () => {
          // console.log(error);
        }
      );
    });
  }

  deleteAllItems() {
    database.transaction(transaction => {
      transaction.executeSql('DELETE FROM items', [], () => {
        this.setState({
          items: []
        });
      });
    });
  }

  updateSearch(searchText) {
    this.setState(
      {
        search: searchText
      },
      () => {
        this.loadItems();
      }
    );
  }

  renderItem({ item }) {
    return (
      <ListItem
        style={{ flex: 1 }}
        title={item.code}
        subtitle={item.id.toString()}
        onPress={() => {
          this.onItemPressed(item);
        }}
        rightIcon={
          <Icon
            name="alpha-x-circle"
            type="material-community"
            color="#FF5733"
            size={32}
            onPress={() => this.deleteItem(item)}
          />
        }
        bottomDivider
      />
    );
  }

  render() {
    const {
      initialGet,
      showOverlay,
      selectedItemId,
      search,
      sortType,
      sortDirection,
      items
    } = this.state;
    let selectedItem;
    if (selectedItemId != null) {
      selectedItem = items.get(selectedItemId);
    }
    return (
      <SafeAreaView style={Style.container}>
        <View style={{ flex: 1 }}>
          <Button
            onPress={() => {
              this.setState({
                initialGet: true
              });
              this.getItems(1000);
            }}
            title={initialGet ? 'Reset Item List' : 'Get Items'}
          />
          <Button
            onPress={() => {
              this.deleteAllItems();
            }}
            title="Delete All Items"
          />
          <SearchBar
            lightTheme
            inputContainerStyle={{ backgroundColor: 'white' }}
            placeholder="Type here to filter"
            onChangeText={this.updateSearch}
            value={search}
          />
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 0.6 }}>
              <Picker
                mode="dropdown"
                selectedValue={sortType}
                onValueChange={value => {
                  this.setState({ sortType: value }, () => {
                    this.loadItems();
                  });
                }}
              >
                <Picker.Item color="#2089dc" label="Item Code" value="code" />
                <Picker.Item color="#2089dc" label="Item ID" value="id" />
              </Picker>
            </View>
            <View style={{ flex: 0.4 }}>
              <Picker
                mode="dropdown"
                selectedValue={sortDirection}
                onValueChange={value => {
                  this.setState({ sortDirection: value }, () => {
                    this.loadItems();
                  });
                }}
              >
                <Picker.Item color="#2089dc" label="Ascending" value="ASC" />
                <Picker.Item color="#2089dc" label="Descending" value="DESC" />
              </Picker>
            </View>
          </View>
          {[...items.keys()].length !== 0 ? (
            <FlatList
              keyExtractor={item => item.id.toString()}
              data={[...items.values()]}
              renderItem={this.renderItem}
            />
          ) : (
            <Text>Press the button to populate the screen.</Text>
          )}
          <Overlay
            isVisible={showOverlay}
            windowBackgroundColor="rgba(255, 255, 255, .5)"
            onBackdropPress={() => this.setState({ showOverlay: false, selectedItemId: null })}
          >
            <View>
              <Text h3>
                Edit details for item with id: {selectedItemId !== null ? selectedItemId : ''}
              </Text>
              <Formik
                enableReinitialize
                initialValues={{
                  code: selectedItem !== undefined ? selectedItem.code : '',
                  desc: selectedItem !== undefined ? selectedItem.desc_01 : ''
                }}
                onSubmit={values => {
                  this.setState({
                    showOverlay: false,
                    selectedItemId: null
                  });
                  database.transaction(transaction => {
                    transaction.executeSql(
                      'UPDATE items SET code=?, desc_01=? WHERE id=?;',
                      [values.code, values.desc, selectedItemId],
                      () => {
                        const newItems = new Map(items);
                        newItems.get(selectedItemId).code = values.code;
                        newItems.get(selectedItemId).desc_01 = values.desc;
                        this.setState({
                          items: newItems
                        });
                      },
                      () => {
                        // console.log(error);
                      }
                    );
                  });
                }}
              >
                {({ values, handleChange, handleSubmit, isSubmitting }) => (
                  <View>
                    <Input
                      label="Item Code"
                      value={values.code}
                      onChangeText={handleChange('code')}
                    />
                    <Input
                      label="Item Description 01"
                      value={values.desc}
                      onChangeText={handleChange('desc')}
                      multiline
                      textAlignVertical="top"
                    />
                    <View style={{ marginTop: 20 }}>
                      <Button loading={isSubmitting} onPress={handleSubmit} title="Save" />
                    </View>
                  </View>
                )}
              </Formik>
            </View>
          </Overlay>
        </View>
      </SafeAreaView>
    );
  }
}

SQLiteDataScreen.propTypes = {
  app: PropTypes.shape({
    username: PropTypes.string,
    token: PropTypes.string,
    apiUrl: PropTypes.string,
    user: PropTypes.shape({
      username: PropTypes.string,
      email: PropTypes.string,
      first_name: PropTypes.string,
      last_name: PropTypes.string,
      timezone: PropTypes.string,
      last_login: PropTypes.string,
      password_changed_at: PropTypes.string
    }),
    successMessage: PropTypes.string,
    errorMessage: PropTypes.string
  })
};

SQLiteDataScreen.defaultProps = {
  app: PropTypes.shape({
    username: '',
    token: '',
    apiUrl: '',
    user: PropTypes.shape({
      username: '',
      email: '',
      first_name: '',
      last_name: '',
      timezone: '',
      last_login: '',
      password_changed_at: ''
    }),
    successMessage: '',
    errorMessage: ''
  })
};

const mapStateToProps = state => ({
  app: state.app
});

const mapDispatchToProps = dispatch => ({
  tokenExpired: message => dispatch(AppActions.tokenExpired(message)),
  passwordExpired: message => dispatch(AppActions.passwordExpired(message))
});

export default connect(mapStateToProps, mapDispatchToProps)(SQLiteDataScreen);
