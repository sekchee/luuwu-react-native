import React from 'react';
import { View, SafeAreaView } from 'react-native';
import { Input, Button, Text } from 'react-native-elements';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { Formik } from 'formik';
import * as Yup from 'yup';
import AppActions from '../../Stores/App/Actions';
import Style from './UserProfileStyle';
import {
  translate,
  setI18nConfig,
  addLocaleChangedListener,
  removeLocaleChangedListener
} from '../../Translations';

class UserProfileScreen extends React.PureComponent {
  constructor() {
    super();

    setI18nConfig(); // set initial config

    this.useOnLocalizationChange = this.useOnLocalizationChange.bind(this);

    this.state = {
      username: '',
      email: '',
      firstName: '',
      lastName: '',
      timezone: '',
      lastLogin: '',
      passwordChangedAt: '',
      currentPassword: '',
      newPassword: '',
      retypeNewPassword: ''
    };
  }

  componentDidMount() {
    addLocaleChangedListener(this.handleLocalizationChange);
  }

  componentDidUpdate() {}

  componentWillUnmount() {
    removeLocaleChangedListener(this.handleLocalizationChange);
  }

  useOnLocalizationChange() {
    setI18nConfig();
    this.forceUpdate();
  }

  render() {
    const {
      username,
      email,
      firstName,
      lastName,
      timezone,
      lastLogin,
      passwordChangedAt,
      currentPassword,
      newPassword,
      retypeNewPassword
    } = this.state;
    const { successMessage, errorMessage, changePassword } = this.props;
    const initialValues = {
      username,
      email,
      firstName,
      lastName,
      timezone,
      lastLogin,
      passwordChangedAt,
      currentPassword,
      newPassword,
      retypeNewPassword
    };

    return (
      <SafeAreaView style={Style.container}>
        {successMessage !== '' && (
          <View style={Style.successContainer}>
            <Text style={Style.successText}>{successMessage}</Text>
          </View>
        )}
        {errorMessage !== '' && (
          <View style={Style.errorContainer}>
            <Text style={Style.errorText}>{errorMessage}</Text>
          </View>
        )}
        <View style={Style.formContainer}>
          <Formik
            enableReinitialize
            initialValues={initialValues}
            onSubmit={(values, formikBag) => {
              // save to state
              this.setState({
                currentPassword: values.currentPassword,
                newPassword: values.newPassword,
                retypeNewPassword: values.retypeNewPassword
              });
              // dispatch the action
              // Use values.currentPassword and values.newPassword because setState is asynchronous
              // Therefore, the currentPassword and newPassword in the state might not have changed when executing this line
              changePassword(formikBag, values.currentPassword, values.newPassword);
            }}
            validationSchema={Yup.object().shape({
              currentPassword: Yup.string().required(translate('current_password_is_required')),
              newPassword: Yup.string().required(translate('new_password_is_required')),
              retypeNewPassword: Yup.string().test(
                'passwords-match',
                translate('passwords_do_not_match'),
                function validate(value) {
                  return this.parent.newPassword === value;
                }
              )
            })}
          >
            {({ values, handleChange, errors, isSubmitting, isValid, handleSubmit }) => (
              <>
                <Input
                  containerStyle={Style.formElement}
                  value={values.currentPassword}
                  onChangeText={handleChange('currentPassword')}
                  placeholder={translate('currentPassword')}
                  secureTextEntry
                  errorMessage={errors.currentPassword}
                  disabled={isSubmitting}
                />
                <Input
                  containerStyle={Style.formElement}
                  value={values.newPassword}
                  onChangeText={handleChange('newPassword')}
                  placeholder={translate('newPassword')}
                  secureTextEntry
                  errorMessage={errors.newPassword}
                  disabled={isSubmitting}
                />
                <Input
                  containerStyle={Style.formElement}
                  value={values.retypeNewPassword}
                  onChangeText={handleChange('retypeNewPassword')}
                  placeholder={translate('retypeNewPassword')}
                  secureTextEntry
                  errorMessage={errors.retypeNewPassword}
                  disabled={isSubmitting}
                />
                <Button
                  containerStyle={Style.formElement}
                  disabled={!isValid}
                  loading={isSubmitting}
                  onPress={handleSubmit}
                  title={translate('changePassword')}
                />
              </>
            )}
          </Formik>
        </View>
      </SafeAreaView>
    );
  }
}

UserProfileScreen.propTypes = {
  successMessage: PropTypes.string,
  errorMessage: PropTypes.string,
  changePassword: PropTypes.func
};

UserProfileScreen.defaultProps = {
  successMessage: '',
  errorMessage: '',
  changePassword: () => {}
};

const mapStateToProps = state => ({
  successMessage: state.app.successMessage,
  errorMessage: state.app.errorMessage
});

const mapDispatchToProps = dispatch => ({
  changePassword: (formikBag, currentPassword, newPassword) =>
    dispatch(AppActions.changePassword(formikBag, currentPassword, newPassword))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileScreen);
