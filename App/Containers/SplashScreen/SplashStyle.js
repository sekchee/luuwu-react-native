import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    margin: 30
  },
  logoContainer: {
    flex: 3,
    alignSelf: 'center',
    marginBottom: 35
  },
  logoImage: {
    width: 150,
    height: 150
  }
});
