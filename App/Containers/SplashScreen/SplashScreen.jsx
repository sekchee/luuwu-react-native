import React from 'react';
import { View } from 'react-native';
import { Image } from 'react-native-elements';
import { Images } from '../../Theme';
import Style from './SplashStyle';

export default class SplashScreen extends React.PureComponent {
  render() {
    return (
      <View style={Style.container}>
        <View style={Style.logo}>
          <Image source={Images.logo} style={Style.logoImage} />
        </View>
      </View>
    );
  }
}
