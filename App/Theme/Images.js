/**
 * Images should be stored in the `App/Images` directory and referenced using variables defined here.
 */

const logo = require('../Assets/Images/logo.png');
const noPhotoIcon = require('../Assets/Images/no-photo-icon.png');
const noPhotoMedium = require('../Assets/Images/no-photo-medium.png');

export default {
  logo,
  noPhotoIcon,
  noPhotoMedium
};
