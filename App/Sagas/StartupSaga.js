import { select } from 'redux-saga/effects';
import NavigationService from '../Services/NavigationService';

/**
 * The startup saga is the place to define behavior to execute when the application starts.
 */

const getAppStore = state => state.app;

export default function* startup() {
  const app = yield select(getAppStore);

  if (app.token === null) {
    NavigationService.navigate('NotLoggedInNavigator');
  } else {
    NavigationService.navigate('LoggedInNavigator');
  }
}
