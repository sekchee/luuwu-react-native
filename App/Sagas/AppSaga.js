import { put, call, select } from 'redux-saga/effects';
import AppActions from '../Stores/App/Actions';
import NavigationService from '../Services/NavigationService';
import ApiService from '../Services/ApiService';

const getAppStore = state => state.app;

export function* authenticate({ formikBag, username, password }) {
  formikBag.setSubmitting(true);
  try {
    const app = yield select(getAppStore);
    const postData = {
      username,
      password
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      'login/authenticate',
      app.token,
      postData // params
    );
    if (result.isSuccess === true) {
      yield put(AppActions.authenticateSuccess(result.data.token, result.message));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.tokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.passwordExpired(result.message));
    } else {
      yield put(AppActions.showErrorMessage(result.message));
    }
  } catch (error) {
    yield put(AppActions.showErrorMessage(error.message));
  } finally {
    formikBag.setSubmitting(false);
  }
}

export function authenticateSuccess() {
  NavigationService.navigate('LoggedInNavigator');
}

export function tokenExpired() {
  NavigationService.navigate('NotLoggedInNavigator');
}

export function passwordExpired() {
  NavigationService.navigate('ChangePasswordScreen');
}

export function* changePassword({ formikBag, currentPassword, newPassword }) {
  formikBag.setSubmitting(true);
  try {
    const app = yield select(getAppStore);
    const postData = {
      currentPassword,
      newPassword
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `auth/changePassword`,
      app.token,
      postData // params
    );
    if (result.isSuccess === true) {
      yield put(AppActions.changePasswordSuccess(result.message));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.tokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.passwordExpired(result.message));
    } else {
      yield put(AppActions.showErrorMessage(result.message));
    }
  } catch (error) {
    yield put(AppActions.showErrorMessage(error.message));
  } finally {
    formikBag.setSubmitting(false);
  }
}
