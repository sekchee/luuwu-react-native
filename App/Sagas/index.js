import { takeLatest, all } from 'redux-saga/effects';
import { StartupTypes } from '../Stores/Startup/Actions';
import { AppTypes } from '../Stores/App/Actions';
import { BarcodeScannerTypes } from '../Stores/BarcodeScanner/Actions';
import { BrochureTypes } from '../Stores/Brochure/Actions';
import { ItemDetailsTypes } from '../Stores/ItemDetails/Actions';
import startup from './StartupSaga';
import { startScanner, scanSuccess } from './BarcodeScannerSaga';
import {
  authenticate,
  authenticateSuccess,
  tokenExpired,
  passwordExpired,
  changePassword
} from './AppSaga';
import { fetchItems, fetchAllItemGroup01s } from './BrochureSaga';
import {
  selectItem,
  fetchItemPhotos,
  selectPhotos,
  uploadItemPhoto,
  uploadItemPhotoSuccess,
  stopUploadItemPhoto
} from './ItemDetailsSaga';
import { openCameraCatalog, goBack } from './CatalogSaga';
import { CatalogTypes } from '../Stores/Catalog/Actions';

export default function* root() {
  yield all([
    /**
     * @see https://redux-saga.js.org/docs/basics/UsingSagaHelpers.html
     */
    takeLatest(StartupTypes.STARTUP, startup),

    takeLatest(AppTypes.AUTHENTICATE, authenticate),
    takeLatest(AppTypes.AUTHENTICATE_SUCCESS, authenticateSuccess),
    takeLatest(AppTypes.TOKEN_EXPIRED, tokenExpired),
    takeLatest(AppTypes.PASSWORD_EXPIRED, passwordExpired),
    takeLatest(AppTypes.CHANGE_PASSWORD, changePassword),

    takeLatest(BarcodeScannerTypes.START_SCANNER, startScanner),
    takeLatest(BarcodeScannerTypes.SCAN_SUCCESS, scanSuccess),

    takeLatest(BrochureTypes.FETCH_ITEMS, fetchItems),
    takeLatest(BrochureTypes.FETCH_ALL_ITEM_GROUP01S, fetchAllItemGroup01s),

    takeLatest(ItemDetailsTypes.FETCH_ITEM_PHOTOS, fetchItemPhotos),
    takeLatest(ItemDetailsTypes.SELECT_ITEM, selectItem),
    takeLatest(ItemDetailsTypes.SELECT_PHOTOS, selectPhotos),
    takeLatest(ItemDetailsTypes.UPLOAD_ITEM_PHOTO, uploadItemPhoto),
    takeLatest(ItemDetailsTypes.UPLOAD_ITEM_PHOTO_SUCCESS, uploadItemPhotoSuccess),
    takeLatest(ItemDetailsTypes.STOP_UPLOAD_ITEM_PHOTO, stopUploadItemPhoto),

    takeLatest(CatalogTypes.SELECT_PHOTOS, openCameraCatalog),
    takeLatest(CatalogTypes.GO_BACK, goBack)
  ]);
}
