import { put, call, select } from 'redux-saga/effects';
import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import BrochureActions from '../Stores/Brochure/Actions';

const getAppStore = state => state.app;

export function* fetchItems({ page, sorts, filters }) {
  try {
    yield put(BrochureActions.fetchItemsLoading(true));

    const processedSorts = [];
    Object.entries(sorts).forEach(([key, value]) => {
      processedSorts.push(`${key}:${value > 0 ? 'ASC' : 'DESC'}`);
    });

    const processedFilters = [];
    Object.entries(filters).forEach(([key, value]) => {
      if (key === 'item_group01_code_in') {
        // it is array
        let valueList = '';
        value.forEach(a => {
          if (valueList.length === 0) {
            valueList += a;
          } else {
            valueList = `${valueList},${a}`;
          }
        });
        if (valueList) {
          processedFilters.push(`${key}:${valueList}`);
        }
      } else if (value) {
        processedFilters.push(`${key}:${value}`);
      }
    });

    const app = yield select(getAppStore);
    const getData = {
      page,
      sorts: processedSorts,
      filters: processedFilters
    };

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `item/indexProcess/ITEM_LIST_02`,
      app.token,
      getData // params
    );
    if (result.isSuccess === true) {
      // if currentPage is more than lastPage, then currentPage = lastPage
      let currentPage = result.data.current_page;
      if (currentPage > result.data.last_page) {
        currentPage = result.data.last_page;
      }
      yield put(
        BrochureActions.fetchItemsSuccess(
          result.data.data,
          currentPage,
          result.data.last_page,
          result.data.total,
          ''
        )
      );
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.tokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.passwordExpired(result.message));
    } else {
      yield put(BrochureActions.showErrorMessage(result.message));
    }
  } catch (error) {
    yield put(BrochureActions.showErrorMessage(error.message));
  } finally {
    yield put(BrochureActions.fetchItemsLoading(false));
  }
}

export function* fetchAllItemGroup01s() {
  try {
    yield put(BrochureActions.fetchAllItemGroup01sLoading(true));

    const app = yield select(getAppStore);
    const getData = {
      pageSize: '10000',
      page: 1
    };

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      'itemGroup01/index',
      app.token,
      getData // params
    );
    if (result.isSuccess === true) {
      yield put(BrochureActions.fetchAllItemGroup01sSuccess(result.data.data, ''));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.tokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.passwordExpired(result.message));
    } else {
      yield put(BrochureActions.showErrorMessage(result.message));
    }
  } catch (error) {
    yield put(BrochureActions.showErrorMessage(error.message));
  } finally {
    yield put(BrochureActions.fetchAllItemGroup01sLoading(false));
  }
}
