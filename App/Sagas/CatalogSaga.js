// import { Platform } from 'react-native';
// import { put, call, select } from 'redux-saga/effects';
// import ApiService from '../Services/ApiService';
// import AppActions from '../Stores/App/Actions';
// import BrochureActions from '../Stores/Brochure/Actions';
// import ItemDetailsActions from '../Stores/ItemDetails/Actions';
import NavigationService from '../Services/NavigationService';

// const getAppStore = state => state.app;

export function selectItem() {
  NavigationService.navigate('ItemDetailsScreen');
}

export function openCameraCatalog() {
  NavigationService.navigate('ItemPhotoUploadScreen');
}

export function goBack() {
  NavigationService.goBack();
}
