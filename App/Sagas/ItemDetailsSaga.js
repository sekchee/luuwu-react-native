import { Platform } from 'react-native';
import { put, call, select } from 'redux-saga/effects';
import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import BrochureActions from '../Stores/Brochure/Actions';
import ItemDetailsActions from '../Stores/ItemDetails/Actions';
import NavigationService from '../Services/NavigationService';

const getAppStore = state => state.app;

export function* fetchItemPhotos({ itemId, page }) {
  try {
    yield put(ItemDetailsActions.fetchItemPhotosLoading(true));

    const app = yield select(getAppStore);
    const getData = {
      page
    };

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `item/showPhotos/${itemId}`,
      app.token,
      getData,
      'multipart/form-data' // params
    );
    if (result.isSuccess === true) {
      // if currentPage is more than lastPage, then currentPage = lastPage
      let currentPage = result.data.current_page;
      if (currentPage > result.data.last_page) {
        currentPage = result.data.last_page;
      }
      yield put(
        ItemDetailsActions.fetchItemPhotosSuccess(
          result.data.data,
          currentPage,
          result.data.last_page,
          result.data.total,
          ''
        )
      );
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.tokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.passwordExpired(result.message));
    } else {
      yield put(ItemDetailsActions.showErrorMessage(result.message));
    }
  } catch (error) {
    yield put(ItemDetailsActions.showErrorMessage(error.message));
  } finally {
    yield put(ItemDetailsActions.fetchItemPhotosLoading(false));
  }
}

export function selectItem() {
  NavigationService.navigate('ItemDetailsScreen');
}

export function selectPhotos() {
  NavigationService.navigate('ItemPhotosUploadScreen');
}

export function* uploadItemPhoto({
  formikBag,
  itemPhotoIndex,
  itemId,
  isFeatured,
  uomId,
  desc01,
  desc02,
  photo
}) {
  formikBag.setSubmitting(true);

  const pathTokens = photo.path.split('/');
  let filename = '';
  if (pathTokens.length > 0) {
    filename = pathTokens[pathTokens.length - 1];
  }
  try {
    const app = yield select(getAppStore);

    // FormData is a global component and therefore must be exempted to avoid eslint warnings
    /* global FormData:false */
    const postData = new FormData();
    postData.append('file', {
      name: filename,
      uri: Platform.OS === 'android' ? photo.path : photo.path.replace('file://', ''),
      type: photo.mime
    });
    postData.append('filename', filename);
    postData.append('isFeatured', isFeatured);
    postData.append('uom_id', uomId);
    postData.append('desc_01', desc01);
    postData.append('desc_02', desc02);

    const getData = {};

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `item/createPhoto/${itemId}`,
      app.token,
      postData,
      getData,
      'multipart/form-data' // params
    );
    if (result.isSuccess === true) {
      yield put(
        ItemDetailsActions.uploadItemPhotoSuccess(
          itemPhotoIndex,
          result.data.data.item,
          result.data.data.itemPhotos
        )
      );
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.tokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.passwordExpired(result.message));
    } else {
      yield put(ItemDetailsActions.showErrorMessage(result.message));
    }
  } catch (error) {
    yield put(ItemDetailsActions.showErrorMessage(error.message));
  } finally {
    formikBag.setSubmitting(false);
  }
}

export function* uploadItemPhotoSuccess({ item }) {
  try {
    yield put(BrochureActions.updateItemPhotoSuccess(item));
  } catch (error) {
    yield put(ItemDetailsActions.showErrorMessage(error.message));
  }
}

export function* stopUploadItemPhoto() {
  try {
    NavigationService.goBack();
  } catch (error) {
    yield put(ItemDetailsActions.showErrorMessage(error.message));
  }
}
