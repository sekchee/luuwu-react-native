import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  selectItem: ['itemId', 'index'],
  resetItemPhotos: null,
  fetchItemPhotos: ['itemId', 'page', 'sorts', 'filters', 'pageSize'],
  fetchItemPhotosLoading: ['boolean'],
  fetchItemPhotosSuccess: ['itemPhotos', 'currentPage', 'lastPage', 'total', 'successMessage'],
  selectPhotos: ['photos'],
  uploadItemPhoto: [
    'formikBag',
    'itemPhotoIndex',
    'itemId',
    'isFeatured',
    'uomId',
    'desc01',
    'desc02',
    'photo'
  ],
  uploadItemPhotoSuccess: ['itemPhotoIndex', 'item', 'itemPhotos'],
  stopUploadItemPhoto: null,
  showErrorMessage: ['errorMessage']
});

export const ItemDetailsTypes = Types;
export default Creators;
