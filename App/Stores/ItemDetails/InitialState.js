/**
 * The initial values for the redux state.
 */
export default {
  itemId: 0,
  itemPhotos: [],
  itemPhotosIsLoading: false,
  itemPhotosSorts: {},
  itemPhotosFilters: {},
  itemPhotosPageSize: 20,
  itemPhotosCurrentPage: 1,
  itemPhotosLastPage: 10,
  itemPhotosTotal: 100,
  selectedPhotos: [],
  successMessage: '',
  errorMessage: ''
};
