/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import INITIAL_STATE from './InitialState';
import { BrochureTypes } from './Actions';

export const resetItems = state => ({
  ...state,
  items: []
});

export const fetchItemsLoading = (state, { boolean }) => ({
  ...state,
  itemsIsLoading: boolean
});

export const fetchItemsSuccess = (
  state,
  { items, currentPage, lastPage, total, successMessage }
) => ({
  ...state,
  items: [...state.items, ...items],
  currentPage,
  lastPage,
  total,
  successMessage,
  errorMessage: ''
});

export const updateItemPhotoSuccess = (state, { item }) => ({
  ...state,
  items: state.items.map(oldItem => {
    if (oldItem.id === item.id) {
      return {
        ...oldItem,
        feat_icon_url: item.feat_icon_url
      };
    }
    return oldItem;
  })
});

export const resetItemGroup01s = state => ({
  ...state,
  itemGroup01s: []
});

export const fetchAllItemGroup01sLoading = (state, { boolean }) => ({
  ...state,
  itemsIsLoading: boolean
});

export const fetchAllItemGroup01sSuccess = (state, { itemGroup01s, successMessage }) => ({
  ...state,
  itemGroup01s: [...state.itemGroup01s, ...itemGroup01s],
  successMessage,
  errorMessage: ''
});

export const setFilters = (state, { filters }) => ({
  ...state,
  filters: {
    ...state.filters,
    ...filters
  }
});

export const resetFilters = state => ({
  ...state,
  filters: INITIAL_STATE.filters
});

export const showErrorMessage = (state, { errorMessage }) => ({
  ...state,
  successMessage: '',
  errorMessage
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [BrochureTypes.RESET_ITEMS]: resetItems,
  [BrochureTypes.FETCH_ITEMS_LOADING]: fetchItemsLoading,
  [BrochureTypes.FETCH_ITEMS_SUCCESS]: fetchItemsSuccess,
  [BrochureTypes.UPDATE_ITEM_PHOTO_SUCCESS]: updateItemPhotoSuccess,
  [BrochureTypes.RESET_ITEM_GROUP01S]: resetItemGroup01s,

  [BrochureTypes.FETCH_ALL_ITEM_GROUP01S_LOADING]: fetchAllItemGroup01sLoading,
  [BrochureTypes.FETCH_ALL_ITEM_GROUP01S_SUCCESS]: fetchAllItemGroup01sSuccess,

  [BrochureTypes.SET_FILTERS]: setFilters,
  [BrochureTypes.RESET_FILTERS]: resetFilters,

  [BrochureTypes.SHOW_ERROR_MESSAGE]: showErrorMessage
});
