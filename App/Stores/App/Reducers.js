/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import INITIAL_STATE from './InitialState';
import { AppTypes } from './Actions';

export const updateApiUrl = (state, { apiUrl }) => ({
  ...state,
  apiUrl
});

export const authenticateSuccess = (state, { token, successMessage }) => ({
  ...state,
  token,
  successMessage,
  errorMessage: ''
});

export const showErrorMessage = (state, { errorMessage }) => ({
  ...state,
  successMessage: '',
  errorMessage
});

export const tokenExpired = (state, { errorMessage }) => ({
  ...state,
  successMessage: '',
  errorMessage
});

export const passwordExpired = (state, { errorMessage }) => ({
  ...state,
  successMessage: '',
  errorMessage
});

export const changePasswordSuccess = (state, { successMessage }) => ({
  ...state,
  successMessage,
  errorMessage: ''
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [AppTypes.UPDATE_API_URL]: updateApiUrl,
  [AppTypes.AUTHENTICATE_SUCCESS]: authenticateSuccess,
  [AppTypes.SHOW_ERROR_MESSAGE]: showErrorMessage,
  [AppTypes.TOKEN_EXPIRED]: tokenExpired,
  [AppTypes.PASSWORD_EXPIRED]: passwordExpired,
  [AppTypes.CHANGE_PASSWORD_SUCCESS]: changePasswordSuccess
});
