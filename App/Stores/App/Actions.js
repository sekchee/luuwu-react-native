import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  updateApiUrl: ['apiUrl'],
  authenticate: ['formikBag', 'username', 'password'],
  authenticateSuccess: ['token', 'successMessage'],
  changePassword: ['formikBag', 'currentPassword', 'newPassword'],
  changePasswordSuccess: ['successMessage'],
  showErrorMessage: ['errorMessage'],
  tokenExpired: ['errorMessage'],
  passwordExpired: ['errorMessage']
});

export const AppTypes = Types;
export default Creators;
