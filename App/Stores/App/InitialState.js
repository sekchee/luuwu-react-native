/**
 * The initial values for the redux state.
 */
export default {
  username: null,
  token: null,
  apiUrl: 'http://inverze.dyndns.org:8000/scm-backend/public/api/v1',
  user: {
    username: '',
    email: '',
    first_name: '',
    last_name: '',
    timezone: '',
    last_login: '',
    password_changed_at: ''
  },
  successMessage: '',
  errorMessage: ''
};
