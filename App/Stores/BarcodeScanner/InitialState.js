/**
 * The initial values for the redux state.
 */
export default {
  token: null,
  barcodes: [],
  scannerIsSuccess: false,
  scannerErrorMessage: null
};
