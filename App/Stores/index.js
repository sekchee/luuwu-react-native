import { combineReducers } from 'redux';
import configureStore from './CreateStore';
import rootSaga from '../Sagas';
import { reducer as AppReducer } from './App/Reducers';
import { reducer as BarcodeScannerReducer } from './BarcodeScanner/Reducers';
import { reducer as BrochureReducer } from './Brochure/Reducers';
import { reducer as ItemDetailsReducer } from './ItemDetails/Reducers';
import { reducer as CatalogReducer } from './Catalog/Reducers';

export default () => {
  const rootReducer = combineReducers({
    /**
     * Register your reducers here.
     * @see https://redux.js.org/api-reference/combinereducers
     */
    app: AppReducer,
    barcodeScanner: BarcodeScannerReducer,
    brochure: BrochureReducer,
    itemDetails: ItemDetailsReducer,
    catalog: CatalogReducer
  });

  return configureStore(rootReducer, rootSaga);
};
