/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import INITIAL_STATE from './InitialState';
import { CatalogTypes } from './Actions';

// export const selectItem = (state, { itemId }) => ({
//   ...state,
//   itemId
// });

// export const resetItemPhotos = state => ({
//   ...state,
//   itemPhotos: []
// });

// export const fetchItemPhotosLoading = (state, { boolean }) => ({
//   ...state,
//   itemPhotosIsLoading: boolean
// });

// export const fetchItemPhotosSuccess = (
//   state,
//   { itemPhotos, currentPage, lastPage, total, successMessage }
// ) => ({
//   ...state,
//   itemPhotos: [...state.itemPhotos, ...itemPhotos],
//   itemPhotosCurrentPage: currentPage,
//   itemPhotosLastPage: lastPage,
//   itemPhotosTotal: total,
//   successMessage,
//   errorMessage: ''
// });

export const selectPhotos = (state, { photos }) => ({
  ...state,
  selectedPhotos: photos
});

export const goBack = state => ({
  ...state
});

// export const uploadItemPhotoSuccess = (state, { itemPhotoIndex, itemPhotos }) => ({
//   ...state,
//   itemPhotos,
//   selectedPhotos: state.selectedPhotos.filter((photo, index) => {
//     if (itemPhotoIndex === index) {
//       // remove the itemPhotoIndex
//       return false;
//     }
//     return true;
//   }),
//   successMessage: 'success',
//   errorMessage: ''
// });

// export const showErrorMessage = (state, { errorMessage }) => ({
//   ...state,
//   successMessage: '',
//   errorMessage
// });

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  //   [CatalogTypes.SELECT_ITEM]: selectItem,
  //   [CatalogTypes.RESET_ITEM_PHOTOS]: resetItemPhotos,
  //   [CatalogTypes.FETCH_ITEM_PHOTOS_LOADING]: fetchItemPhotosLoading,
  //   [CatalogTypes.FETCH_ITEM_PHOTOS_SUCCESS]: fetchItemPhotosSuccess,
  [CatalogTypes.SELECT_PHOTOS]: selectPhotos,
  [CatalogTypes.GO_BACK]: goBack
  //   [CatalogTypes.UPLOAD_ITEM_PHOTO_SUCCESS]: uploadItemPhotoSuccess,
  //   [CatalogTypes.SHOW_ERROR_MESSAGE]: showErrorMessage
});
