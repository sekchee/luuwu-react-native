import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  selectPhotos: ['photos'],
  goBack: []
});

export const CatalogTypes = Types;
export default Creators;
