import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import LoggedInNavigator from './LoggedInNavigator';
import NotLoggedInNavigator from './NotLoggedInNavigator';
import SplashScreen from '../Containers/SplashScreen/SplashScreen';

export default createAppContainer(
  createSwitchNavigator(
    {
      SplashScreen,
      LoggedInNavigator,
      NotLoggedInNavigator
    },
    {
      initialRouteName: 'SplashScreen'
    }
  )
);
