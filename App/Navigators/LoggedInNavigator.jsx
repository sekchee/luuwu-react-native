import React from 'react';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Icon } from 'react-native-elements';
import { createStackNavigator } from 'react-navigation-stack';

import { PropTypes } from 'prop-types';
import BrochureScreen from '../Containers/Brochure/BrochureScreen';
import ItemDetailsScreen from '../Containers/ItemDetails/ItemDetailsScreen';
import ItemPhotosUploadScreen from '../Containers/ItemPhotosUpload/ItemPhotosUploadScreen';
import ChangePasswordScreen from '../Containers/ChangePassword/ChangePasswordScreen';
import BarcodeScannerScreen from '../Containers/BarcodeScanner/BarcodeScannerScreen';
import { translate, setI18nConfig } from '../Translations';
import RealmDataScreen from '../Containers/RealmData/RealmDataScreen';
import SQLiteDataScreen from '../Containers/SQLiteData/SQLiteDataScreen';
import CatalogScreen from '../Containers/Catalog/CatalogScreen';

setI18nConfig();

function generateTabBarIconFunction(iconName, iconType) {
  function TabBarIcon({ tintColor }) {
    return <Icon name={iconName} type={iconType} size={25} style={{ color: tintColor }} />;
  }

  TabBarIcon.propTypes = {
    tintColor: PropTypes.string.isRequired
  };
  return TabBarIcon;
}
const HomeStackNavigator = createStackNavigator(
  {
    BrochureScreen,
    ItemDetailsScreen,
    ItemPhotosUploadScreen,
    BarcodeScannerScreen
  },
  {
    initialRouteName: 'BrochureScreen',
    headerMode: 'none'
  }
);

const CatalogStackNavigator = createStackNavigator(
  {
    CatalogScreen,
    ItemDetailsScreen,
    ItemPhotosUploadScreen,
    BarcodeScannerScreen
  },
  {
    initialRouteName: 'CatalogScreen',
    headerMode: 'none'
  }
);

const AccountStackNavigator = createStackNavigator(
  {
    ChangePasswordScreen
  },
  {
    initialRouteName: 'ChangePasswordScreen',
    headerMode: 'none'
  }
);

const LoggedInNavigator = createBottomTabNavigator(
  {
    HomeStack: {
      navigationOptions: {
        tabBarIcon: generateTabBarIconFunction('home', 'material-community'),
        tabBarLabel: translate('home')
      },
      screen: HomeStackNavigator
    },
    InfoStack: {
      navigationOptions: {
        tabBarIcon: generateTabBarIconFunction('account', 'material-community'),
        tabBarLabel: translate('info')
      },
      screen: AccountStackNavigator
    },
    Realm: {
      navigationOptions: {
        tabBarIcon: generateTabBarIconFunction('database', 'material-community'),
        tabBarLabel: 'Realm'
      },
      screen: RealmDataScreen
    },
    SQLite: {
      navigationOptions: {
        tabBarIcon: generateTabBarIconFunction('database', 'material-community'),
        tabBarLabel: 'SQLite'
      },
      screen: SQLiteDataScreen
    },
    Catalog: {
      navigationOptions: {
        tabBarIcon: generateTabBarIconFunction('database', 'material-community'),
        tabBarLabel: translate('catalog')
      },
      screen: CatalogStackNavigator
    }
  },
  {
    initialRouteName: 'HomeStack'
  }
);

export default LoggedInNavigator;
