import { createStackNavigator } from 'react-navigation-stack';

import LoginScreen from '../Containers/Login/LoginScreen';
import BarcodeScannerScreen from '../Containers/BarcodeScanner/BarcodeScannerScreen';

const NotLoggedInNavigator = createStackNavigator(
  {
    LoginScreen,
    BarcodeScannerScreen
  },
  {
    initialRouteName: 'LoginScreen',
    headerMode: 'none'
  }
);

export default NotLoggedInNavigator;
