import React from 'react';
import { View } from 'react-native';

import PropTypes from 'prop-types';
import SelectButton from './SelectButton';
import Style from './MultiSelectButtonStyle';

const IOS_BLUE = '#007AFF';

class MultiSelectButton extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      selectedValue: []
    };

    this.useOnSingleTap = this.useOnSingleTap.bind(this);

    this.isSelected = this.isSelected.bind(this);
  }

  componentDidMount() {
    // populate selectedValue
    const newSelectedValue = [];
    const { selectedValue, multiple } = this.props;
    if (selectedValue !== undefined) {
      selectedValue.forEach(value => {
        if (newSelectedValue.length > 1) {
          if (multiple) {
            newSelectedValue.push(value);
          }
        } else {
          newSelectedValue.push(value);
        }
      });
    }
    this.setState({
      selectedValue: newSelectedValue
    });
  }

  componentDidUpdate() {}

  componentWillUnmount() {}

  useOnSingleTap(valueTap) {
    let newSelectedValue = [];
    const { multiple, onValueChange, singleTap } = this.props;
    const { selectedValue } = this.state;
    if (multiple) {
      if (selectedValue.includes(valueTap)) {
        newSelectedValue = selectedValue.filter(curValue => {
          return curValue !== valueTap;
        });
      } else {
        newSelectedValue = [...selectedValue, valueTap];
      }
    } else {
      newSelectedValue = [valueTap];
    }

    onValueChange(newSelectedValue);

    this.setState({
      selectedValue: newSelectedValue
    });

    singleTap(valueTap);
  }

  isSelected(value) {
    const { selectedValue } = this.state;
    return selectedValue.includes(value);
  }

  render() {
    const { containerStyle, items, buttonStyle, textStyle, highlightStyle, multiple } = this.props;

    return (
      <View style={[Style.container, containerStyle]}>
        {items.map(ele => (
          <SelectButton
            key={ele.value}
            buttonStyle={buttonStyle}
            textStyle={textStyle}
            highlightStyle={highlightStyle}
            multiple={multiple}
            value={ele.value}
            label={ele.label}
            selected={this.isSelected(ele.value)}
            singleTap={this.useOnSingleTap}
          />
        ))}
      </View>
    );
  }
}

MultiSelectButton.propTypes = {
  multiple: PropTypes.bool,
  selectedValue: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])),

  items: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
      label: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    })
  ).isRequired,

  highlightStyle: PropTypes.shape({
    borderColor: PropTypes.string.isRequired,
    backgroundColor: PropTypes.string.isRequired,
    textColor: PropTypes.string.isRequired,
    borderTintColor: PropTypes.string.isRequired,
    backgroundTintColor: PropTypes.string.isRequired,
    textTintColor: PropTypes.string.isRequired
  }),
  containerStyle: PropTypes.shape(),
  buttonStyle: PropTypes.shape(),
  textStyle: PropTypes.shape(),

  singleTap: PropTypes.func,
  onValueChange: PropTypes.func
};

MultiSelectButton.defaultProps = {
  multiple: true,
  singleTap: () => {},
  onValueChange: () => {},
  highlightStyle: {
    borderColor: 'gray',
    backgroundColor: 'transparent',
    textColor: 'gray',
    borderTintColor: IOS_BLUE,
    backgroundTintColor: 'transparent',
    textTintColor: IOS_BLUE
  },
  containerStyle: {},
  buttonStyle: {},
  textStyle: {},
  selectedValue: []
};

export default MultiSelectButton;
