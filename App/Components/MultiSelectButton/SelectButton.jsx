import React from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';

import Style from './SelectButtonStyle';

const IOS_BLUE = '#007AFF';

class SelectButton extends React.PureComponent {
  componentDidMount() {}

  render() {
    const {
      singleTap,
      value,
      buttonStyle,
      highlightStyle,
      textStyle,
      label,
      selected
    } = this.props;
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          singleTap(value);
        }}
      >
        <View
          style={[
            Style.button,
            buttonStyle,
            {
              borderColor: selected ? highlightStyle.borderTintColor : highlightStyle.borderColor,
              backgroundColor: selected
                ? highlightStyle.backgroundTintColor
                : highlightStyle.backgroundColor
            }
          ]}
        >
          <Text
            style={[
              Style.text,
              textStyle,
              {
                color: selected ? highlightStyle.textTintColor : highlightStyle.textColor
              }
            ]}
          >
            {label === undefined ? value : label}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

SelectButton.propTypes = {
  selected: PropTypes.bool,

  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  highlightStyle: PropTypes.shape({
    borderColor: PropTypes.string.isRequired,
    backgroundColor: PropTypes.string.isRequired,
    textColor: PropTypes.string.isRequired,
    borderTintColor: PropTypes.string.isRequired,
    backgroundTintColor: PropTypes.string.isRequired,
    textTintColor: PropTypes.string.isRequired
  }),

  buttonStyle: PropTypes.shape(),
  textStyle: PropTypes.shape(),
  singleTap: PropTypes.func
};

SelectButton.defaultProps = {
  selected: false,
  highlightStyle: {
    borderColor: 'gray',
    backgroundColor: 'transparent',
    textColor: 'gray',
    borderTintColor: IOS_BLUE,
    backgroundTintColor: 'transparent',
    textTintColor: IOS_BLUE
  },
  buttonStyle: {},
  textStyle: {},

  label: 0,

  singleTap: () => {}
};

export default SelectButton;
