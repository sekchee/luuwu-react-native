# LuuWu React Native Boilerplate

- create new project, react-native init ProjectName
- create App folder
- move ./App.js to ./App/

## 1) Naming Convention

[NAMING_CONVENTION.md](./NAMING_CONVENTION.md)

## 2) Change Project Name

```
npm install react-native-rename -g
```

## 3) React Navigation

[REACT_NAVIGATION.md](./REACT_NAVIGATION.md)

## 4) Redux Persist

[REDUX_PERSIST.md](./REDUX_PERSIST.md)

## 5) React Redux, Redux Saga and axios

[REDUX_SAGA.md](./REDUX_SAGA.md)

## 6) Formik

```
npm install formik
npm install yup
```

**withFormik() not working in react native**

## 7) React Native Camera

[REACT_NATIVE_CAMERA.md](./REACT_NATIVE_CAMERA.md)

## 8) React Native Internationalization

[MULTI_LANGUAGE.md](./MULTI_LANGUAGE.md)

## 9) React Native Image Crop Picker

we use react-native-image-crop-picker, instead of react-native-image-picker, because it support more features, like can support multiple picture selection
[REACT_NATIVE_IMAGE_CROP_PICKER.md](./REACT_NATIVE_IMAGE_CROP_PICKER.md)

## 10) React Keystore

[REACT_KEYSTORE.md](./REACT_KEYSTORE.md)
