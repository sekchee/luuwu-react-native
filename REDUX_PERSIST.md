```
npm install redux-persist
```
- for version 6: Users must now explicitly pass their storage engine

```
import { AsyncStorage } from '@react-native-community/async-storage'

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  /**
   * Blacklist state that we do not need/want to persist
   */
  blacklist: [
    // 'app',
  ],
}
```
- AsyncStorage has been extracted from react-native core and will be removed in a future release
```
npm install @react-native-community/async-storage
```