- Generate keystore using Android Studio
- [How to generate key and keystore using Android Studio](https://developer.android.com/studio/publish/app-signing#generate-key)
- Copy generated keystore to android/app
- Update android/app/build.gradle with keystore's alias, store password and key password

```
    signingConfigs {
        debug {
            storeFile file('xxx.keystore')
            storePassword 'myStorePassword'
            keyAlias 'myAlias'
            keyPassword 'myPassword'
        }
    }

```

- Run ./gradlew assembleRelease from /android
- Generated apk can be found in /android/app/build/outputs/apk/release
