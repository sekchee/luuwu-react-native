# List of unresolved formatting warnings

### 1) BarcodeScannerScreen.jsx

- jsx-one-expression-per-line

### 2) BrochureScreen.jsx

- jsx-wrap-multilines
- forbid-prop-types (No idea how 'sorts' look like)

### 3) CameraScreen.jsx

- jsx-one-expression-per-line

### 4) ItemDetailsScreen.jsx

- jsx-wrap-multilines

### 5) ItemPhotosUploadScreen.jsx

- jsx-wrap-multilines

### 6) LoginScreen.jsx

- jsx-wrap-multilines

### 7) RealmDataScreen.jsx

- jsx-wrap-multilines
- jsx-one-expression-per-line

### 8) SQLiteDataScreen.jsx

- jsx-wrap-multilines
- jsx-one-expression-per-line

## Notes:

- Correcting jsx-one-expression-per-line will lead to a another warning, fixing the other warning will lead back to the original warning

- Correcting jsx-wrap-multilines will lead to a another warning, fixing the other warning will lead back to the original warning
