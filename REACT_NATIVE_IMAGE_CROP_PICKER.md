```
npm install react-native-image-crop-picker
```

# IOS

# Android
- Make sure you are using Gradle >= 2.2.x (android/build.gradle)
```
buildscript {
    ...
    dependencies {
        classpath 'com.android.tools.build:gradle:2.2.3'
        ...
    }
    ...
}
```

- VERY IMPORTANT Add the following to your build.gradle's repositories section. (android/build.gradle)
```
allprojects {
    repositories {
      mavenLocal()
      jcenter()
      maven { url "$rootDir/../node_modules/react-native/android" }

      // ADD THIS
      maven { url 'https://maven.google.com' }

      // ADD THIS
      maven { url "https://www.jitpack.io" }
    }
}
```

- Add useSupportLibrary (android/app/build.gradle)
```
android {
    ...

    defaultConfig {
        ...
        vectorDrawables.useSupportLibrary = true
        ...
    }
    ...
}
```

- Use Android SDK >= 26 (android/app/build.gradle)
```
android {
    compileSdkVersion 27
    buildToolsVersion "27.0.3"
    ...
    
    defaultConfig {
      ...
      targetSdkVersion 27
      ...
    }
    ...
}
```

- [Optional] If you want to use camera picker in your project, add following to app/src/main/AndroidManifest.xml
```
<uses-permission android:name="android.permission.CAMERA"/>
```

- [Optional] If you want to use front camera, also add following to app/src/main/AndroidManifest.xml
```
<uses-feature android:name="android.hardware.camera" android:required="false" />
<uses-feature android:name="android.hardware.camera.front" android:required="false" />
```

- refer to, https://github.com/ivpusic/react-native-image-crop-picker