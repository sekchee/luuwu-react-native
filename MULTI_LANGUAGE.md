* install the required packages
* react-native-i18n is deprecated in favor of react-native-localize
```
npm install react-native-localize
npm install i18n-js
npm install lodash.memoize
```

* create \App\Translations\ folder, and en.json language file, sample as following:
```
{
    "username": "Username",
    "password": "Password",
}
```

* inside Translations folder, create index.js file:
```
import * as RNLocalize from 'react-native-localize';
import i18n from 'i18n-js';
import memoize from 'lodash.memoize';
import { I18nManager } from 'react-native';

const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require('./en.json'),
};

export const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);

export const setI18nConfig = () => {
    // fallback if no available language fits
    const fallback = { languageTag: 'en', isRTL: false };

    const { languageTag, isRTL } = RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) || fallback;

    // clear translation cache
    translate.cache.clear();
    // update layout direction
    I18nManager.forceRTL(isRTL);
    // set i18n-js config
    i18n.translations = { [languageTag]: translationGetters[languageTag]() };
    i18n.locale = languageTag;
};

export const addLocaleChangedListener = (handler) => {
    RNLocalize.addEventListener('change', handler);
}

export const removeLocaleChangedListener = (handler) => {
    RNLocalize.removeEventListener('change', handler);
}
```

* to enable multi-language in container, can follow App/Containers/Login/LoginScreen:
```
import { translate, setI18nConfig, addLocaleChangedListener, removeLocaleChangedListener } from '../../Translations';
```

```
constructor() {
    super();

    setI18nConfig(); // set initial config

    ...

    this.useOnLocalizationChange = this.useOnLocalizationChange.bind(this);
}
```

```
useOnLocalizationChange() {
    setI18nConfig();
    this.forceUpdate();
};

componentDidMount() {
    addLocaleChangedListener(this.handleLocalizationChange);
}

componentWillUnmount() {
    removeLocaleChangedListener(this.handleLocalizationChange);
}
```
* use translate function for multi-language
```
this.props.showErrorMessage(translate('incorrect_qr_code_format'));
```