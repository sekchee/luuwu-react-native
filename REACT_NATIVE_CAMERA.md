```
npm install react-native-camera
react-native link react-native-camera
```

# IOS
- add following line in info.plist
```
    <key>NSCameraUsageDescription</key>
    <string>To enable camera permission</string>
    <key>NSMicrophoneUsageDescription</key>
    <string>To enable camera permission</string>
```
- add the following line into Podfile
```
 pod 'react-native-camera', path: '../node_modules/react-native-camera', subspecs: [
  'TextDetector',
  'FaceDetectorMLKit',
  'BarcodeDetectorMLKit'
]
```
- then run "cd ios && pod install"
-	Register your app in Firebase console.
-	Download GoogleService-Info.plist and add it to your project
-	Add pod 'Firebase/Core' to your podfile
-	In your AppDelegate.m file add the following lines:
```
#import <Firebase.h> // <--- add this
...

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  [FIRApp configure]; // <--- add this
  ...
}
```
# Android
- can refer to https://github.com/react-native-community/react-native-camera/tree/master/examples/mlkit

- Append the following lines to android/settings.gradle
```
include ':react-native-camera'
project(':react-native-camera').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-camera/android')
```

- Insert the following lines in android/app/build.gradle
  - inside the dependencies block:
  ```
  implementation project(':react-native-camera')
  ```
  - using MLKit for text/face/barcode recognition:
  ```
  android {
    ...
    defaultConfig {
      ...
      missingDimensionStrategy 'react-native-camera', 'mlkit' <-- insert this line
    }
  }
  ```

- Declare the permissions in your Android Manifest (required for video recording feature)
```
<uses-permission android:name="android.permission.RECORD_AUDIO"/>
<uses-permission android:name="android.permission.CAMERA" />
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
```

- Add jitpack to android/build.gradle
```
allprojects {
    repositories {
        maven { url "https://jitpack.io" }
        maven { url "https://maven.google.com" }
    }
}
```

- Additional steps for using MLKit for text/face/barcode recognition
  - Using Firebase MLKit requires seting up Firebase project for your app. If you have not already added Firebase to your app, please follow the steps described in getting started guide. In short, you would need to

    - Register your app in Firebase console.
    - Download google-services.json and place it in android/app/
    - add the folowing to project level build.gradle:
    ```
    buildscript {
      dependencies {
        // Add this line
        classpath 'com.google.gms:google-services:4.3.2' <-- you might want to use different version
      }
    }
    ```
    - add the folowing to project level build.gradle:
    ```
    apply plugin: 'com.google.gms.google-services'
    ```

  - AndroidManifest.xml, Configure your app to automatically download the ML model to the device after your app is installed from the Play Store. If you do not enable install-time model downloads, the model will be downloaded the first time you run the on-device detector. Requests you make before the download has completed will produce no results.
  ```
  <application>
    <meta-data
        android:name="com.google.firebase.ml.vision.DEPENDENCIES"
        android:value="ocr" />
    <!-- To use multiple models, list all needed models: android:value="ocr, face, barcode" -->
  </application>
  ```

  - @react-native-community/slider
  ```
  npm install @react-native-community/slider
  ```